CREATE DATABASE  IF NOT EXISTS `smartman` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `smartman`;

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `authority` varchar(255) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(200) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;



CREATE TABLE `project` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `description` mediumtext,
  `name` varchar(50) NOT NULL,
  `project_type` varchar(10) DEFAULT NULL,
  `short_name` varchar(10) DEFAULT NULL,
  `project_creator_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3bw5gy7ruk9m879w2tcnvxm85` (`project_creator_id`),
  CONSTRAINT `FK3bw5gy7ruk9m879w2tcnvxm85` FOREIGN KEY (`project_creator_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

CREATE TABLE `version` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `release_date` datetime NOT NULL,
  `released` bit(1) NOT NULL DEFAULT b'0',
  `start_date` datetime NOT NULL,
  `project_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK5q7csydn4alo2pf0bbv74g9ko` (`project_id`),
  CONSTRAINT `FK5q7csydn4alo2pf0bbv74g9ko` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;


CREATE TABLE `sprint` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `close_date` datetime DEFAULT NULL,
  `deadline` datetime NOT NULL,
  `description` mediumtext,
  `name` varchar(50) NOT NULL,
  `start_date` datetime NOT NULL,
  `project_id` bigint(20) NOT NULL,
  `version_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKerwve0blrvfhqm1coxo69f0xr` (`project_id`),
  KEY `FKmfcpmkhoswfmqrh18cnufhbyl` (`version_id`),
  CONSTRAINT `FKerwve0blrvfhqm1coxo69f0xr` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `FKmfcpmkhoswfmqrh18cnufhbyl` FOREIGN KEY (`version_id`) REFERENCES `version` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

CREATE TABLE `epic` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `description` mediumtext,
  `name` varchar(50) NOT NULL,
  `priority` varchar(10) NOT NULL,
  `status` varchar(15) NOT NULL,
  `summary` varchar(255) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `assignee_id` bigint(20) DEFAULT NULL,
  `project_id` bigint(20) NOT NULL,
  `reporter_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKsublw7kovfd7oafohana7cid9` (`assignee_id`),
  KEY `FKj6wn7xcnmotfjj5tkpq2b1qkh` (`project_id`),
  KEY `FKayr3lrw4xy6o4cdll06dogdtr` (`reporter_id`),
  CONSTRAINT `FKayr3lrw4xy6o4cdll06dogdtr` FOREIGN KEY (`reporter_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKj6wn7xcnmotfjj5tkpq2b1qkh` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `FKsublw7kovfd7oafohana7cid9` FOREIGN KEY (`assignee_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `task_list` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `project_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKg7pdwgsrcllnfqbqn1ks9622n` (`project_id`),
  CONSTRAINT `FKg7pdwgsrcllnfqbqn1ks9622n` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `issue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `description` mediumtext,
  `type` varchar(10) NOT NULL,
  `priority` varchar(10) NOT NULL,
  `status` varchar(15) DEFAULT NULL,
  `story_points` int(11) DEFAULT NULL,
  `summary` varchar(255) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `assignee_id` bigint(20) DEFAULT NULL,
  `epic_id` bigint(20) DEFAULT NULL,
  `project_id` bigint(20) NOT NULL,
  `reporter_id` bigint(20) DEFAULT NULL,
  `sprint_id` bigint(20) DEFAULT NULL,
  `task_list_id` bigint(20) DEFAULT NULL,
  `version_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK9n0or1k33u8yp5gp578tkl8qd` (`assignee_id`),
  KEY `FK51l72dljet1ege6tmun5p41g1` (`epic_id`),
  KEY `FKcombytcpeogaqi2012phvvvhy` (`project_id`),
  KEY `FKntg62m8393lmu5fnr93vkqa1w` (`reporter_id`),
  KEY `FK5k39vkuty9g6w3n7iwye1nh0i` (`sprint_id`),
  KEY `FKgarl58ytt3d1s2x027ydemeua` (`task_list_id`),
  KEY `FKf4v5wpc7khriip0vptwg84rxp` (`version_id`),
  CONSTRAINT `FK51l72dljet1ege6tmun5p41g1` FOREIGN KEY (`epic_id`) REFERENCES `epic` (`id`),
  CONSTRAINT `FK5k39vkuty9g6w3n7iwye1nh0i` FOREIGN KEY (`sprint_id`) REFERENCES `sprint` (`id`),
  CONSTRAINT `FK9n0or1k33u8yp5gp578tkl8qd` FOREIGN KEY (`assignee_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKcombytcpeogaqi2012phvvvhy` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `FKf4v5wpc7khriip0vptwg84rxp` FOREIGN KEY (`version_id`) REFERENCES `version` (`id`),
  CONSTRAINT `FKgarl58ytt3d1s2x027ydemeua` FOREIGN KEY (`task_list_id`) REFERENCES `task_list` (`id`),
  CONSTRAINT `FKntg62m8393lmu5fnr93vkqa1w` FOREIGN KEY (`reporter_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

CREATE TABLE `project_user` (
  `user_id` bigint(20) NOT NULL,
  `project_id` bigint(20) NOT NULL,
  `role` varchar(20) NOT NULL,
  PRIMARY KEY (`user_id`,`project_id`),
  KEY `FK4ug72llnm0n7yafwntgdswl3y` (`project_id`),
  CONSTRAINT `FK4jl2o131jivd80xsuw6pivnbx` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK4ug72llnm0n7yafwntgdswl3y` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `team` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `team_project` (
  `team_id` bigint(20) NOT NULL,
  `project_id` bigint(20) NOT NULL,
  PRIMARY KEY (`team_id`,`project_id`),
  KEY `FKgueu8qabva2799btmxjcgpv50` (`project_id`),
  CONSTRAINT `FKelq27u0x7m0ku0t7sg5pfdqi0` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`),
  CONSTRAINT `FKgueu8qabva2799btmxjcgpv50` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `team_user` (
  `team_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`team_id`,`user_id`),
  KEY `FK6w6lkqjk13n0nmf4jbnb3d376` (`user_id`),
  CONSTRAINT `FK6w6lkqjk13n0nmf4jbnb3d376` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKiuwi96twuthgvhnarqj34mnjv` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `contents` mediumtext NOT NULL,
  `edited` bit(1) NOT NULL DEFAULT b'0',
  `type` varchar(15) NOT NULL,
  `epic_id` bigint(20) DEFAULT NULL,
  `issue_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKqox5ua5eko9c9ws37s2b99q5e` (`epic_id`),
  KEY `FKomjg70m9sundkar1el2rtonrn` (`issue_id`),
  KEY `FK8kcum44fvpupyw6f5baccx25c` (`user_id`),
  CONSTRAINT `FK8kcum44fvpupyw6f5baccx25c` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKomjg70m9sundkar1el2rtonrn` FOREIGN KEY (`issue_id`) REFERENCES `issue` (`id`),
  CONSTRAINT `FKqox5ua5eko9c9ws37s2b99q5e` FOREIGN KEY (`epic_id`) REFERENCES `epic` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

