package com.epam.smartman.config;

import static com.wix.mysql

        .EmbeddedMysql.anEmbeddedMysql;
import static com.wix.mysql.ScriptResolver.classPathScript;
import static com.wix.mysql.config.Charset.UTF8;
import static com.wix.mysql.config.MysqldConfig.aMysqldConfig;
import static com.wix.mysql.distribution.Version.v5_7_latest;

import com.wix.mysql.EmbeddedMysql;
import com.wix.mysql.config.MysqldConfig;
import java.util.concurrent.TimeUnit;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestDatabaseConfig {

  @Bean
  public EmbeddedMysql createDataSource() {
    MysqldConfig config = aMysqldConfig(v5_7_latest)
        .withCharset(UTF8)
        .withPort(3310)
        .withTimeZone("Europe/Kiev")
        .withTimeout(2, TimeUnit.MINUTES)
        .withServerVariable("max_connect_errors", 666)
        .build();
    return anEmbeddedMysql(config)
        .addSchema("smartman", classPathScript("data/SmartMan.sql"))
        .start();
  }
}
