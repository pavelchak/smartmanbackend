package com.epam.smartman.DB;

import com.epam.smartman.config.ScriptLoaderConfiguration;
import com.epam.smartman.config.TestDatabaseConfig;
import com.wix.mysql.EmbeddedMysql;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.sql.*;

import static com.wix.mysql.ScriptResolver.classPathScript;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
@Ignore
@RunWith(SpringRunner.class)
@Import(TestDatabaseConfig.class)
 public class DataBaseTest {

  @Autowired
  EmbeddedMysql embeddedMysql;

  @InjectMocks
  private ScriptLoaderConfiguration scriptLoaderConfiguration;

  @Mock
  private DataSource dataSource;

  @Before
  public void setUp() {
    //Resetting schemas between tests
    embeddedMysql.reloadSchema("smartman", classPathScript("data/SmartMan.sql"));
  }

  @Test
  public void testConnection() throws SQLException {
    Connection connection = DriverManager.getConnection(
        "jdbc:mysql://localhost:3310/smartman?useSSL=false", "root", "");
    when(dataSource.getConnection()).thenReturn(connection);
    scriptLoaderConfiguration.loadSqlScript();
    Statement statement = connection.createStatement();
    ResultSet rs = statement.executeQuery("SELECT COUNT(*) FROM project");
    int count = 0;
    while (rs.next()) {
      count = rs.getInt(1);
    }
    assertEquals(count, 6);
  }
}

