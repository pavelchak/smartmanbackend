package com.epam.smartman.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.epam.smartman.domain.User;
import com.epam.smartman.domain.enums.Authority;
import com.epam.smartman.service.UserService;
import com.epam.smartman.util.EntityFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

  private final Long USER_ID = 1L;

  private MockMvc mockMvc;

  @InjectMocks
  private UserController userController;
  @Mock
  private UserService userService;

  private User user;

  @Before
  public void setUp() {
    mockMvc = MockMvcBuilders.standaloneSetup(userController)
            .setControllerAdvice(new ExceptionHandlerController())
            .build();
    user = EntityFactory
            .createUser(USER_ID, "Boiko", "Vitalii", null, "vitalii.boiko26@gmail.com", null,
                    Authority.ROLE_USER, true, true, true, true);
  }

  @Test
  public void getUser() throws Exception {
    when(userService.getUser(USER_ID)).thenReturn(user);
    mockMvc.perform(get("/api/user/{userId}", USER_ID)
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().json("{" +
                    "\"name\": \"Vitalii\"," +
                    "\"surname\": \"Boiko\"," +
                    "\"email\": \"vitalii.boiko26@gmail.com\"," +
                    "\"userId\": 1," +
                    "\"fullName\": \"Vitalii Boiko\"," +
                    "\"links\": [{\"rel\":\"self\",\"href\":\"http://localhost/api/user/1\"  }]}"));
  }
}



