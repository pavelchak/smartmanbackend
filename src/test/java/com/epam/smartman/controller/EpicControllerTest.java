package com.epam.smartman.controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.epam.smartman.domain.Epic;
import com.epam.smartman.domain.Project;
import com.epam.smartman.domain.User;
import com.epam.smartman.domain.enums.Authority;
import com.epam.smartman.domain.enums.ProjectRole;
import com.epam.smartman.exceptions.NoAccessRightsException;
import com.epam.smartman.exceptions.NotAuthorisedUserException;
import com.epam.smartman.service.AccessService;
import com.epam.smartman.service.EpicService;
import com.epam.smartman.service.ProjectService;
import com.epam.smartman.util.EntityFactory;
import java.util.ArrayList;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
public class EpicControllerTest {

  private final Long EPIC_ID = 1L;
  private final Long SPRINT_ID = 1L;
  private final Long PROJECT_ID = 1L;
  private final Long USER_ID = 1L;

  private MockMvc mockMvc;

  @InjectMocks
  private EpicController epicController;
  @Mock
  private EpicService epicService;
  @Mock
  ProjectService projectService;
  @Mock
  AccessService accessService;

  private Project project;
  private Epic epic;
  private User user;


  @Before
  public void setUp() throws NotAuthorisedUserException, NoAccessRightsException {
    mockMvc = MockMvcBuilders.standaloneSetup(epicController)
        .setControllerAdvice(new ExceptionHandlerController())
        .build();
    user = EntityFactory
        .createUser(USER_ID, "Boiko", "Vitalii", null, "vitalii.boiko26@gmail.com", null,
            Authority.ROLE_USER, true, true, true, true);
    project = EntityFactory.createProject(PROJECT_ID, "SmartMan", "SM", "", user, null, null);
    epic = EntityFactory
        .createEpic(EPIC_ID, "", "", new Date(), new Date(), "", null, null, null, null, project);
    when(projectService.getProject(PROJECT_ID)).thenReturn(project);
    doNothing().when(accessService).checkAccessRights(project, ProjectRole.READER);
  }

  @Test
  public void getEpic() throws Exception {
    when(epicService.readEpic(EPIC_ID)).thenReturn(epic);
    mockMvc.perform(get("/api/epic/{epicId}", PROJECT_ID, SPRINT_ID)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void getAllEpics() throws Exception {
    when(epicService.getAllEpics()).thenReturn(new ArrayList<>());
    mockMvc.perform(get("/api/epic")
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void deleteEpic() throws Exception {
    doNothing().when(epicService).deleteEpic(EPIC_ID);
    mockMvc.perform(delete("/api/epic/{epicId}",EPIC_ID)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }
}
