package com.epam.smartman.controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.epam.smartman.domain.Project;
import com.epam.smartman.domain.Sprint;
import com.epam.smartman.domain.User;
import com.epam.smartman.domain.enums.Authority;
import com.epam.smartman.domain.enums.ProjectRole;
import com.epam.smartman.exceptions.NoAccessRightsException;
import com.epam.smartman.exceptions.NotAuthorisedUserException;
import com.epam.smartman.service.AccessService;
import com.epam.smartman.service.ProjectService;
import com.epam.smartman.service.SprintService;
import com.epam.smartman.util.EntityFactory;
import java.util.ArrayList;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
public class SprintControllerTest {

  private final Long USER_ID = 1L;
  private final Long SPRINT_ID = 1L;
  private final Long PROJECT_ID = 1L;
  private final Long VERSION_ID = 1L;

  private MockMvc mockMvc;

  @InjectMocks
  private SprintController sprintController;
  @Mock
  private SprintService sprintService;
  @Mock
  ProjectService projectService;
  @Mock
  AccessService accessService;

  private Sprint sprint;
  private Project project;
  private User user;

  @Before
  public void setUp() throws NotAuthorisedUserException, NoAccessRightsException {
    mockMvc = MockMvcBuilders.standaloneSetup(sprintController)
        .setControllerAdvice(new ExceptionHandlerController())
        .build();
    user = EntityFactory
        .createUser(USER_ID, "Boiko", "Vitalii", null, "vitalii.boiko26@gmail.com", null,
            Authority.ROLE_USER, true, true, true, true);
    project = EntityFactory.createProject(PROJECT_ID, "SmartMan", "SM", "", user, null, null);
    sprint = EntityFactory
        .createSprint(SPRINT_ID, "", new Date(), new Date(), new Date(), "", project, null);
    when(projectService.getProject(PROJECT_ID)).thenReturn(project);
    doNothing().when(accessService).checkAccessRights(project, ProjectRole.READER);
  }

  @Test
  public void getSprint() throws Exception {
    when(sprintService.getSprint(project, SPRINT_ID)).thenReturn(sprint);
    mockMvc.perform(get("/api/project/{projectId}/sprint/{sprintId}", PROJECT_ID, SPRINT_ID)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void getProjectSprints() throws Exception {
    when(sprintService.getProjectSprints(project)).thenReturn(new ArrayList<>());
    mockMvc.perform(get("/api/project/{projectId}/sprint", PROJECT_ID)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void getProjectSprint() throws Exception {
    when(sprintService.getSprint(project, SPRINT_ID)).thenReturn(sprint);
    mockMvc.perform(get("/api/project/{projectId}/sprint/{sprintId}", PROJECT_ID, SPRINT_ID)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void closeSprint() throws Exception {
    when(sprintService.closeSprint(project, SPRINT_ID)).thenReturn(sprint);
    mockMvc.perform(put("/api/project/{projectId}/closesprint/{sprintId}", PROJECT_ID, SPRINT_ID)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void deleteSprint() throws Exception {
    doNothing().when(sprintService).deleteSprint(project, SPRINT_ID);
    mockMvc.perform(delete("/api/project/{projectId}/sprint/{sprintId}", PROJECT_ID, SPRINT_ID)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }
}
