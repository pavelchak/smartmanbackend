package com.epam.smartman.controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.epam.smartman.domain.Project;
import com.epam.smartman.domain.Sprint;
import com.epam.smartman.domain.Team;
import com.epam.smartman.domain.User;
import com.epam.smartman.domain.enums.Authority;
import com.epam.smartman.domain.enums.ProjectRole;
import com.epam.smartman.exceptions.NoAccessRightsException;
import com.epam.smartman.exceptions.NotAuthorisedUserException;
import com.epam.smartman.service.AccessService;
import com.epam.smartman.service.ProjectService;
import com.epam.smartman.service.SprintService;
import com.epam.smartman.service.TeamService;
import com.epam.smartman.util.EntityFactory;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
public class TeamControllerTest {

  private final Long TEAM_ID = 1L;

  private MockMvc mockMvc;

  @InjectMocks
  private TeamController teamController;
  @Mock
  private TeamService teamService;

  private Team team;

  @Before
  public void setUp() throws NotAuthorisedUserException, NoAccessRightsException {
    mockMvc = MockMvcBuilders.standaloneSetup(teamController)
        .setControllerAdvice(new ExceptionHandlerController())
        .build();
    team = EntityFactory.createTeam(TEAM_ID, "", new HashSet<>(), new HashSet<>());
  }

  @Test
  public void getAllTeams() throws Exception {
    when(teamService.getAllTeams()).thenReturn(new ArrayList<>());
    mockMvc.perform(get("/api/team")
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void getTeam() throws Exception {
    when(teamService.readTeam(TEAM_ID)).thenReturn(team);
    mockMvc.perform(get("/api/team/{teamId}", TEAM_ID)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void deleteTeam() throws Exception {
    doNothing().when(teamService).deleteTeam(TEAM_ID);
    mockMvc.perform(get("/api/team")
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }
}
