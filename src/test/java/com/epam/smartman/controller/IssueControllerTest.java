package com.epam.smartman.controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.epam.smartman.domain.Issue;
import com.epam.smartman.domain.Project;
import com.epam.smartman.domain.Sprint;
import com.epam.smartman.domain.User;
import com.epam.smartman.domain.enums.Authority;
import com.epam.smartman.domain.enums.IssueType;
import com.epam.smartman.domain.enums.Priority;
import com.epam.smartman.domain.enums.ProjectRole;
import com.epam.smartman.domain.enums.Status;
import com.epam.smartman.exceptions.NoAccessRightsException;
import com.epam.smartman.exceptions.NotAuthorisedUserException;
import com.epam.smartman.service.AccessService;
import com.epam.smartman.service.IssueService;
import com.epam.smartman.service.ProjectService;
import com.epam.smartman.util.EntityFactory;
import java.util.ArrayList;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
public class IssueControllerTest {

  private final Long USER_ID = 1L;
  private final Long SPRINT_ID = 1L;
  private final Long PROJECT_ID = 1L;
  private final Long VERSION_ID = 1L;
  private final Long ISSUE_ID = 1L;

  private MockMvc mockMvc;

  @InjectMocks
  private IssueController issueController;
  @Mock
  private IssueService issueService;
  @Mock
  ProjectService projectService;
  @Mock
  AccessService accessService;

  private Issue issue;
  private Project project;
  private User user;
  private Sprint sprint;

  @Before
  public void setUp() throws NotAuthorisedUserException, NoAccessRightsException {
    mockMvc = MockMvcBuilders.standaloneSetup(issueController)
        .setControllerAdvice(new ExceptionHandlerController())
        .build();
    user = EntityFactory
        .createUser(USER_ID, "Boiko", "Vitalii", null, "vitalii.boiko26@gmail.com", null,
            Authority.ROLE_USER, true, true, true, true);
    project = EntityFactory.createProject(PROJECT_ID, "SmartMan", "SM", "", user, null, null);
    sprint = EntityFactory
        .createSprint(SPRINT_ID, "", new Date(), new Date(), new Date(), "", project, null);

    issue = EntityFactory
        .createIssue(ISSUE_ID, "", new Date(), new Date(), "", 1, Status.BACKLOG, user, user,
            Priority.LOW, sprint, project, IssueType.BUG, null, null);
    when(projectService.getProject(PROJECT_ID)).thenReturn(project);
    doNothing().when(accessService).checkAccessRights(project, ProjectRole.READER);
  }

  @Test
  public void getIssuesByProjectId() throws Exception {
    when(issueService.getIssuesByProjectId(project)).thenReturn(new ArrayList<>());
    mockMvc.perform(get("/api/project/{projectId}/dashboard", PROJECT_ID)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void getProjectBacklog() throws Exception {
    when(issueService.getProjectBacklog(project)).thenReturn(new ArrayList<>());
    mockMvc.perform(get("/api/project/{projectId}/backlog", PROJECT_ID)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void getProjectIssue() throws Exception {
    when(issueService.getIssue(ISSUE_ID)).thenReturn(issue);
    mockMvc.perform(get("/api/project/{projectId}/issue/{issueId}", PROJECT_ID, ISSUE_ID)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void getBacklogSprint() throws Exception {
    when(issueService.getSprintBacklog(project, SPRINT_ID)).thenReturn(new ArrayList<>());
    mockMvc.perform(get("/api/project/{projectId}/sprint/{sprintId}/issue", PROJECT_ID, SPRINT_ID)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void getSprintIssue() throws Exception {
    when(issueService.getIssue(ISSUE_ID)).thenReturn(issue);
    mockMvc.perform(
        get("/api/project/{projectId}/sprint/{sprintId}/issue/{issueId}", PROJECT_ID, SPRINT_ID,
            ISSUE_ID)
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void deleteIssue() throws Exception {
    doNothing().when(issueService).deleteIssue(ISSUE_ID);
    mockMvc.perform(delete("/api/project/{projectId}/issue/{issueId}", PROJECT_ID, ISSUE_ID)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

}
