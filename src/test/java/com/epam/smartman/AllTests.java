package com.epam.smartman;

import com.epam.smartman.controller.SprintControllerTest;
import com.epam.smartman.controller.TeamControllerTest;
import com.epam.smartman.controller.UserControllerTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(value=Suite.class)
@Suite.SuiteClasses(value={SprintControllerTest.class, UserControllerTest.class,
        TeamControllerTest.class})
public class AllTests {

}
