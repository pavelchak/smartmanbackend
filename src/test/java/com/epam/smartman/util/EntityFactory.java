package com.epam.smartman.util;

import com.epam.smartman.domain.*;
import com.epam.smartman.domain.enums.*;

import java.util.Date;
import java.util.List;
import java.util.Set;

public class EntityFactory {

  public static User createUser(Long userId, String surname, String username, String password,
      String email, Set<Team> teams, Authority authority, boolean accountNonExpired,
      boolean accountNonLocked, boolean credentialsNonExpired, boolean enabled) {
    User user = new User();
    user.setId(userId);
    user.setSurname(surname);
    user.setUsername(username);
    user.setPassword(password);
    user.setEmail(email);
    user.setTeams(teams);
    user.setAuthority(authority);

    return user;
  }

  public static Project createProject(Long projectId, String name, String shortName,
      String description, User projectCreator, Set<Team> teams, List<ProjectUser> projectUsers) {
    Project project = new Project();
    project.setId(projectId);
    project.setName(name);
    project.setShortName(shortName);
    project.setDescription(description);
    project.setProjectCreator(projectCreator);
    project.setTeams(teams);
    project.setProjectUsers(projectUsers);
    return project;
  }

  public static Sprint createSprint(Long sprintId, String name, Date startDate, Date deadline,
      Date closeDate, String description, Project project, Version version) {
    Sprint sprint = new Sprint();
    sprint.setId(sprintId);
    sprint.setName(name);
    sprint.setStartDate(startDate);
    sprint.setDeadline(deadline);
    sprint.setCloseDate(closeDate);
    sprint.setDescription(description);
    sprint.setProject(project);
    sprint.setVersion(version);
    return sprint;
  }

  public static Comment createComment(Long sprintId, String contents, Boolean edited, User user,
      CommentType type, Issue issue, Epic epic) {
    Comment comment = new Comment();
    comment.setId(sprintId);
    comment.setContents(contents);
    comment.setEdited(edited);
    comment.setUser(user);
    comment.setType(type);
    comment.setIssue(issue);
    comment.setEpic(epic);
    return comment;
  }

  public static Credentials createCredentials(String email, String password) {
    Credentials credentials = new Credentials();
    credentials.setUsername(email);
    credentials.setPassword(password);
    return credentials;
  }

  public static Epic createEpic(Long epicId, String name, String summary, Date created,
      Date updated, String description, Status status, User reporter, User assignee,
      Priority priority, Project project) {
    Epic epic = new Epic();
    epic.setId(epicId);
    epic.setName(name);
    epic.setSummary(summary);
    epic.setCreated(created);
    epic.setUpdated(updated);
    epic.setDescription(description);
    epic.setStatus(status);
    epic.setReporter(reporter);
    epic.setAssignee(assignee);
    epic.setPriority(priority);
    epic.setProject(project);
    return epic;
  }

  public static Issue createIssue(Long issueId, String summary, Date created, Date updated,
      String description, Integer storyPoints, Status status, User reporter, User assignee,
      Priority priority, Sprint sprint, Project project, IssueType issueType, Epic epic,
      Version version) {
    Issue issue = new Issue();
    issue.setId(issueId);
    issue.setSummary(summary);
    issue.setCreated(created);
    issue.setUpdated(updated);
    issue.setDescriprion(description);
    issue.setStoryPoints(storyPoints);
    issue.setStatus(status);
    issue.setReporter(reporter);
    issue.setAssignee(assignee);
    issue.setPriority(priority);
    issue.setSprint(sprint);
    issue.setProject(project);
    issue.setIssueType(issueType);
    issue.setEpic(epic);
    issue.setVersion(version);
    return issue;
  }

  public static ProjectUser createProjectUser(Long projectId, Long userId, Project project,
      User user,
      ProjectRole projectRole) {
    ProjectUser projectUser = new ProjectUser();
    projectUser.setProjectId(projectId);
    projectUser.setUserId(userId);
    projectUser.setProject(project);
    projectUser.setUser(user);
    projectUser.setProjectRole(projectRole);
    return projectUser;
  }

  public static Team createTeam(Long teamId, String name, Set<Project> projects, Set<User> users) {
    Team team = new Team();
    team.setId(teamId);
    team.setName(name);
    team.setProjects(projects);
    team.setUsers(users);
    return team;
  }

  public static Version createVersion(Long versionId, String name, String description,
      Date startDate, Date releaseDate, Boolean released, Project project) {
    Version version = new Version();
    version.setId(versionId);
    version.setName(name);
    version.setDescription(description);
    version.setStartDate(startDate);
    version.setReleaseDate(releaseDate);
    version.setReleased(released);
    version.setProject(project);
    return version;
  }

}


