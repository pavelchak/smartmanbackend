package com.epam.smartman.service;

import com.epam.smartman.domain.Project;
import com.epam.smartman.domain.ProjectUser;

import com.epam.smartman.domain.User;
import com.epam.smartman.domain.enums.ProjectRole;
import com.epam.smartman.exceptions.NotAuthorisedUserException;
import java.util.List;

public interface ProjectService {

  List<Project> getAllProjects();

  Project createProject(Project project) throws NotAuthorisedUserException;

  List<Project> getUserProjects(User user);

  Project getProject(Long id);

  Project updateProject(Project project, Project updProject);

  void deleteProject(Project project);

  Project addTeam(Long id, Long teamId);

  Project removeTeam(Long id, Long teamId);

  void addUserToProject(Project project, Long userId, ProjectRole projectRole);

  void removeUserFromProject(Project project, Long userId);

  List<ProjectUser> getProjectUsers(Project project);

  void changeProjectUserRole(Project project, Long userId, ProjectRole projectRole);

}