package com.epam.smartman.service;

import com.epam.smartman.domain.User;

import java.util.List;

public interface UserService {

    List<User> getAllUsers();

    User getUser(Long userId);

    User updateUser(User updatedUser, Long userId);

    void deleteUser(Long userId);

    User findByEmail(String email);

    void save(User user);

    boolean isUserExist(User user);

    User findById(Long id);

    List<User> getUsersLike(String pattern);

    User addTeam(Long userId, Long teamID);

    void removeTeam(Long userId, Long teamID);

}
