package com.epam.smartman.service;

import com.epam.smartman.domain.Project;
import com.epam.smartman.domain.TaskList;
import java.util.List;

public interface TaskListService {

    List<TaskList> getTaskLists(Project project);

    TaskList addTaskList(Project project, String nameTaskList);

    TaskList changeTaskListName(Long taskListId, String taskListName);

    void addScrumTaskLists(Project project);

}