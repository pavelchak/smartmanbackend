package com.epam.smartman.service;

import com.epam.smartman.domain.Issue;
import com.epam.smartman.domain.User;

public interface MailSendService {

    void sendEmail(User user);
    void sendNotification(Issue issue);
}
