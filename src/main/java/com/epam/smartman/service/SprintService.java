package com.epam.smartman.service;

import com.epam.smartman.domain.Project;
import com.epam.smartman.domain.Sprint;
import com.epam.smartman.exceptions.NotAllowedOperationException;
import java.util.List;

public interface SprintService {

  List<Sprint> getProjectSprints(Project project);

  Sprint getSprint(Project project, Long sprintId);

  Sprint createSprint(Project project, Long versionId, Sprint sprint)
      throws NotAllowedOperationException;

  Sprint updateSprint(Project project, Sprint updatedSprint, Long sprintId, Long versionId);

  void deleteSprint(Project project, Long sprintId);

  Long getCurrentSprint(Project project);

  Sprint closeSprint(Project project, Long sprintId) throws NotAllowedOperationException;
}
