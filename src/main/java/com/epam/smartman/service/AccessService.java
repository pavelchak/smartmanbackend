package com.epam.smartman.service;

import com.epam.smartman.domain.Project;
import com.epam.smartman.domain.User;
import com.epam.smartman.domain.enums.ProjectRole;
import com.epam.smartman.exceptions.NoAccessRightsException;
import com.epam.smartman.exceptions.NotAuthorisedUserException;

public interface AccessService {

    User getAuthorizedUser() throws NotAuthorisedUserException;

    void checkAccessRights(Project project, ProjectRole projectRole)
            throws NotAuthorisedUserException, NoAccessRightsException;

    boolean checkAuthorityForAdmin() throws NotAuthorisedUserException;
}
