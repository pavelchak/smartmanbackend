package com.epam.smartman.service;

import com.epam.smartman.domain.Team;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TeamService {
    List<Team> getAllTeams();

    Team createTeam(Team team);

    Team readTeam(Long id);

    Team updateTeam(Long id, Team team);

    void deleteTeam(Long id);

}
