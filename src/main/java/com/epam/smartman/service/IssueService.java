package com.epam.smartman.service;

import com.epam.smartman.controller.DTO.IssueDTO;
import com.epam.smartman.domain.Issue;
import com.epam.smartman.domain.Project;
import com.epam.smartman.exceptions.NotAuthorisedUserException;

import java.util.List;

public interface IssueService {

    List<Issue> getIssuesByProjectId(Project project);

    List<Issue> getProjectBacklog(Project project);

    Issue getIssue(Long issueId);

    List<Issue> getSprintBacklog(Project project, Long sprintId);

    void deleteIssue(Long issueId);

    Issue createIssue(Project project, IssueDTO newIssue) throws NotAuthorisedUserException;

    Issue updateIssue(IssueDTO newIssue, Long issueId);

    Issue updateIssueStatus(Long issueId, String newStatus);

    Issue updateIssueTaskList(Long issueId, Long taskList);

    List<Issue> getTaskList(Project project, Long taskListId);
}