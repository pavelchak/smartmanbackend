package com.epam.smartman.service.Impl;

import com.epam.smartman.domain.Project;
import com.epam.smartman.domain.ProjectUser;
import com.epam.smartman.domain.Team;
import com.epam.smartman.domain.User;
import com.epam.smartman.domain.enums.Authority;
import com.epam.smartman.domain.enums.ProjectRole;
import com.epam.smartman.exceptions.NotAuthorisedUserException;
import com.epam.smartman.repository.ProjectRepository;
import com.epam.smartman.repository.ProjectUserRepository;
import com.epam.smartman.repository.TeamRepository;
import com.epam.smartman.repository.UserRepository;
import com.epam.smartman.service.AccessService;
import com.epam.smartman.service.ProjectService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Optional;

import java.util.Set;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class ProjectServiceImpl implements ProjectService {

  @Autowired
  ProjectRepository projectRepository;
  @Autowired
  TeamRepository teamRepository;
  @Autowired
  AccessService accessService;
  @Autowired
  UserRepository userRepository;
  @Autowired
  ProjectUserRepository projectUserRepository;

  @Override
  public List<Project> getAllProjects() {
    return projectRepository.findAll();
  }

  @Override
  public List<Project> getUserProjects(User user) {
    if (user.getAuthority() == Authority.ROLE_ADMIN) {
      return projectRepository.findAll();
    } else {
      return projectRepository.findProjectsByUser(user);
    }
  }

  @Override
  @Transactional
  public Project createProject(Project project) throws NotAuthorisedUserException {
    User user = accessService.getAuthorizedUser();
    project.setProjectCreator(user);
    Project newProject = projectRepository.save(project);
    if (user.getAuthority() != Authority.ROLE_ADMIN) {
      addUserToProject(newProject, user.getId(), ProjectRole.CREATOR);
    }
    return newProject;
  }

  @Override
  public Project getProject(Long id) {
    return Optional.ofNullable(projectRepository.findOne(id))
        .orElseThrow(() -> new NoSuchElementException("No such project present"));
  }

  @Transactional
  @Override
  public Project updateProject(Project project, Project updProject) {
    project.setDescription(updProject.getDescription());
    project.setName(updProject.getName());
    project.setShortName(updProject.getShortName());
    project.setTeams(updProject.getTeams());
    project.setActive(updProject.isActive());
    return projectRepository.save(project);
  }

  @Override
  @Transactional
  public void addUserToProject(Project project, Long userId, ProjectRole projectRole) {
    User user = Optional.ofNullable(userRepository.findOne(userId))
        .orElseThrow(() -> new NoSuchElementException("Such user is not present"));
    if (projectUserRepository.findByProjectAndUser(project, user) != null) {
      throw new IllegalArgumentException("Such user is added in project already");
    }
    ProjectUser projectUser = new ProjectUser();
    projectUser.setProject(project);
    projectUser.setUser(user);
    projectUser.setProjectId(project.getId());
    projectUser.setUserId(user.getId());
    projectUser.setProjectRole(projectRole);
    projectUserRepository.save(projectUser);
  }

  @Override
  @Transactional
  public void removeUserFromProject(Project project, Long userId) {
    User user = Optional.ofNullable(userRepository.findOne(userId))
        .orElseThrow(() -> new NoSuchElementException("Such user is not present"));
    ProjectUser projectUser = Optional
        .ofNullable(projectUserRepository.findByProjectAndUser(project, user))
        .orElseThrow(() -> new NoSuchElementException("Such user is not present in Project"));
    projectUserRepository.delete(projectUser);
  }

  @Override
  @Transactional
  public void changeProjectUserRole(Project project, Long userId, ProjectRole projectRole) {
    User user = Optional.ofNullable(userRepository.findOne(userId))
        .orElseThrow(() -> new NoSuchElementException("Such user is not present"));
    ProjectUser projectUser = Optional
        .ofNullable(projectUserRepository.findByProjectAndUser(project, user))
        .orElseThrow(() -> new NoSuchElementException("Such user is not present in project"));
    projectUser.setProjectRole(projectRole);
    projectUserRepository.save(projectUser);
  }

  @Override
  public List<ProjectUser> getProjectUsers(Project project) {
    return projectUserRepository.findUsersByProject(project);
  }

  @Transactional
  @Override
  public void deleteProject(Project project) {
    projectRepository.delete(project);
  }

  @Transactional
  @Override
  public Project addTeam(Long id, Long teamID) {
    Project project = getProject(id);
    Team team = Optional.ofNullable(teamRepository.findOne(teamID))
        .orElseThrow(() -> new NoSuchElementException("No such team found"));
    team.getProjects().add(project);
    teamRepository.save(team);
    //add users from the Team to ProjectUser Table
    for (User user : team.getUsers()) {
      if (projectUserRepository.findByProjectAndUser(project, user) == null) {
        ProjectUser projectUser = new ProjectUser();
        projectUser.setProject(project);
        projectUser.setUser(user);
        projectUser.setProjectId(project.getId());
        projectUser.setUserId(user.getId());
        projectUser.setProjectRole(ProjectRole.WRITER);
        projectUserRepository.save(projectUser);
      }
    }
    return projectRepository.getOne(id);
  }

  @Transactional
  @Override
  public Project removeTeam(Long id, Long teamID) {
    Project project = getProject(id);
    Team currentTeam = Optional.ofNullable(teamRepository.findOne(teamID))
        .orElseThrow(() -> new NoSuchElementException("No such team found"));
    Set<User> userListForNotRemoveFromProjectUser = new HashSet<>();

    for (Team otherProjectTeam : project.getTeams()) {
      if (otherProjectTeam != currentTeam) {
        userListForNotRemoveFromProjectUser.addAll(otherProjectTeam.getUsers());
      }
    }

    List<User> userListForRemove = new LinkedList<>();
    for (User user : currentTeam.getUsers()) {
      if (!userListForNotRemoveFromProjectUser.contains(user)) {
        userListForRemove.add(user);
      }
    }

    for (User userForRemove : userListForRemove) {
      ProjectUser projectUserForRemove = projectUserRepository
          .findByProjectAndUser(project, userForRemove);
      if (projectUserForRemove != null) {
        projectUserRepository.delete(projectUserForRemove);
      }
    }

    currentTeam.getProjects().remove(project);
    teamRepository.save(currentTeam);
    return projectRepository.getOne(id);
  }
}
