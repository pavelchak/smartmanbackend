package com.epam.smartman.service.Impl;

import com.epam.smartman.domain.Team;
import com.epam.smartman.domain.User;
import com.epam.smartman.repository.TeamRepository;
import com.epam.smartman.service.TeamService;
import java.util.HashSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;


@Service
public class TeamServiceImpl implements TeamService {

  @Autowired
  private TeamRepository teamRepository;

  @Autowired
  public TeamServiceImpl(TeamRepository teamRepository) {
    this.teamRepository = teamRepository;
  }

  @Override
  public List<Team> getAllTeams() {
    return teamRepository.findAll();
  }

  @Transactional
  @Override
  public Team createTeam(Team team) {
    team.setUsers(new HashSet<>());
    return teamRepository.save(team);
  }

  @Override
  public Team readTeam(Long id) {
    return Optional.ofNullable(teamRepository.findOne(id))
        .orElseThrow(() -> new NoSuchElementException("No such team found"));//1.5.10
  }

  @Override
  public Team updateTeam(Long id, Team team) {
    Team updTeam = Optional.ofNullable(teamRepository.findOne(id))
        .orElseThrow(() -> new NoSuchElementException("No such team found"));//1.5.10

    updTeam.setId(team.getId());

    updTeam.setName(team.getName());

    updTeam.setProjects(team.getProjects());

    updTeam.setUsers(team.getUsers());

    return teamRepository.save(updTeam);
  }

  @Override
  public void deleteTeam(Long id) {
    Team team = Optional.ofNullable(teamRepository.findOne(id))
        .orElseThrow(() -> new NoSuchElementException("No such team found"));//1.5.10

    teamRepository.delete(team);
  }

}
