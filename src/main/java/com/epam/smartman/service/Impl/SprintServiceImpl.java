package com.epam.smartman.service.Impl;

import com.epam.smartman.domain.Project;
import com.epam.smartman.domain.Sprint;
import com.epam.smartman.domain.Version;
import com.epam.smartman.exceptions.NotAllowedOperationException;
import com.epam.smartman.repository.ProjectRepository;
import com.epam.smartman.repository.SprintRepository;
import com.epam.smartman.repository.VersionRepository;
import com.epam.smartman.service.SprintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class SprintServiceImpl implements SprintService {

  @Autowired
  SprintRepository sprintRepository;
  @Autowired
  ProjectRepository projectRepository;
  @Autowired
  VersionRepository versionRepository;

  @Override
  public List<Sprint> getProjectSprints(Project project) {
    return sprintRepository.findSprintsByProject(project);
  }

  @Override
  public Sprint getSprint(Project project, Long sprintId) {
    return Optional.ofNullable(sprintRepository.findOne(sprintId))
        .orElseThrow(() -> new NoSuchElementException("No such sprint found"));
  }

  @Override
  public Long getCurrentSprint(Project project) {
    Sprint sprint = sprintRepository.findByProjectAndCloseDateNull(project);
    return sprint == null ? null : sprint.getId();
  }

  @Transactional
  @Override
  public Sprint createSprint(Project project, Long versionId, Sprint sprint)
      throws NotAllowedOperationException {
    if (sprintRepository.findByProjectAndCloseDateNull(project) != null) {
      throw new NotAllowedOperationException("The current sprint is not closed");
    }
    Version version = versionRepository.findOne(versionId);
    sprint.setStartDate(new Date());
    sprint.setProject(project);
    sprint.setVersion(version);
    return sprintRepository.save(sprint);
  }

  @Transactional
  @Override
  public Sprint updateSprint(Project project, Sprint updatedSprint, Long sprintId, Long versionId) {
    Sprint sprint = Optional.ofNullable(sprintRepository.findOne(sprintId))
        .orElseThrow(() -> new NoSuchElementException("No such sprint found"));
    Version version = versionRepository.findOne(versionId);
    sprint.setDeadline(updatedSprint.getDeadline());
    sprint.setDescription(updatedSprint.getDescription());
    sprint.setName(updatedSprint.getName());
    sprint.setVersion(version);
    return sprintRepository.save(sprint);
  }

  @Transactional
  @Override
  public Sprint closeSprint(Project project, Long sprintId) throws NotAllowedOperationException {
    Sprint sprint = Optional.ofNullable(sprintRepository.findOne(sprintId))
        .orElseThrow(() -> new NoSuchElementException("No such sprint found"));
    if (sprint.getCloseDate() != null) {
      throw new NotAllowedOperationException("The current sprint is closed");
    }
    sprint.setCloseDate(new Date());
    return sprintRepository.save(sprint);
  }

  @Transactional
  @Override
  public void deleteSprint(Project project, Long sprintId) {
    Sprint sprint = Optional.ofNullable(sprintRepository.findOne(sprintId))
        .orElseThrow(() -> new NoSuchElementException("No such sprint found"));
    sprintRepository.delete(sprint);
  }
}
