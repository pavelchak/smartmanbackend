package com.epam.smartman.service.Impl;

import com.epam.smartman.domain.Issue;
import com.epam.smartman.domain.User;
import com.epam.smartman.service.MailSendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class MailSendServiceImpl implements MailSendService {


    @Autowired
    private JavaMailSender javaMailSender;


    @Override
    public void sendEmail(User user) {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        try {
            helper.setTo(user.getEmail());
            helper.setText(user.getEmail() + " " + user.getPassword()  , true);
            helper.setFrom("smartman.epam@gmail.com");

        } catch (MessagingException e) {
            e.printStackTrace();
        }

        javaMailSender.send(mimeMessage);

    }
    public void sendNotification(Issue issue){

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);

        try {
            helper.setTo(issue.getAssignee().getEmail());
            helper.setText("<h1>Task that are assigned to you were changed!</h1><a href='localhost:8090'>" , true);

            helper.setFrom("smartman.epam@gmail.com");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        javaMailSender.send(mimeMessage);
    }
}

