package com.epam.smartman.service.Impl;

import com.epam.smartman.domain.Project;
import com.epam.smartman.domain.ProjectUser;
import com.epam.smartman.domain.User;
import com.epam.smartman.domain.enums.Authority;
import com.epam.smartman.domain.enums.ProjectRole;
import com.epam.smartman.exceptions.NoAccessRightsException;
import com.epam.smartman.exceptions.NotAuthorisedUserException;
import com.epam.smartman.repository.ProjectUserRepository;
import com.epam.smartman.repository.UserRepository;
import com.epam.smartman.service.AccessService;
import com.epam.smartman.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccessServiceImpl implements AccessService {
  @Autowired
  ProjectService projectService;
  @Autowired
  UserRepository userRepository;
  @Autowired
  ProjectUserRepository projectUserRepository;

  @Override
  public User getAuthorizedUser() throws NotAuthorisedUserException {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    String userName = authentication.getName();
    return Optional.ofNullable(userRepository.findUserByEmail(userName))
            .orElseThrow(NotAuthorisedUserException::new);
  }

  @Override
  public void checkAccessRights(Project project, ProjectRole projectRole)
          throws NotAuthorisedUserException, NoAccessRightsException {
    User user = getAuthorizedUser();
    if(user.getAuthority() == Authority.ROLE_ADMIN) {
      return;
    }
    if (project.getProjectCreator() == user) {
      return;
    }
    ProjectUser projectUser = Optional
            .ofNullable(projectUserRepository.findByProjectAndUser(project, user))
            .orElseThrow(() -> new NoAccessRightsException("No access rights"));
    switch (projectRole) {
      case READER:
        return;
      case WRITER:
        if (projectUser.getProjectRole() == ProjectRole.READER) {
          throw new NoAccessRightsException("No access rights");
        }
        break;
      case CREATOR:
        if (projectUser.getProjectRole() != ProjectRole.CREATOR) {
          throw new NoAccessRightsException("No access rights");
        }
        break;
    }
  }

  @Override
  public boolean checkAuthorityForAdmin() throws NotAuthorisedUserException {
    User userAdmName = getAuthorizedUser();
    return userAdmName.getAuthority() == Authority.ROLE_ADMIN;
  }

}
