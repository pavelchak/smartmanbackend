package com.epam.smartman.service.Impl;

import com.epam.smartman.domain.Team;
import com.epam.smartman.domain.User;
import com.epam.smartman.repository.TeamRepository;
import com.epam.smartman.repository.UserRepository;
import com.epam.smartman.service.UserService;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    TeamRepository teamRepository;

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUser(Long userId) {
        User user = Optional.ofNullable(userRepository.findOne(userId))
                .orElseThrow(() -> new NoSuchElementException("Such user is not present"));
        return user;
    }

    @Transactional
    @Override
    public User updateUser(User updatedUser, Long userId) {
        User user = Optional.ofNullable(userRepository.findOne(userId))
                .orElseThrow(() -> new NoSuchElementException("Such user is not present"));
        user.setSurname(updatedUser.getSurname());
        user.setUsername(updatedUser.getUsername());
        user.setEmail(updatedUser.getEmail());
        user.setAuthority(updatedUser.getAuthority());

        return userRepository.save(user);
    }

    @Transactional
    @Override
    public void deleteUser(Long userId) {
        User user = Optional.ofNullable(userRepository.findOne(userId))
                .orElseThrow(() -> new NoSuchElementException("Such user is not present"));
        userRepository.delete(user);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    @Override
    @Transactional
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public boolean isUserExist(User user) {
        return findByEmail(user.getEmail()) != null;
    }

    @Override
    public User findById(Long id) {
        return userRepository.findOne(id);
    }


    @Override
    public List<User> getUsersLike(String pattern) {
        return userRepository.findUsersBySurnameAndNameLike(pattern);
    }

    @Transactional
    @Override
    public User addTeam(Long userId, Long teamID) {
        User user = Optional.ofNullable(userRepository.findOne(userId))
                .orElseThrow(() -> new NoSuchElementException("No such user found"));
        Team team = Optional.ofNullable(teamRepository.findOne(teamID))
                .orElseThrow(() -> new NoSuchElementException("No such team found"));
        team.getUsers().add(user);
        teamRepository.save(team);
        return userRepository.getOne(userId);
    }

    @Override
    @Transactional
    public void removeTeam(Long userId, Long teamID) {
        User user = Optional.ofNullable(userRepository.findOne(userId))
                .orElseThrow(() -> new NoSuchElementException("No such user found"));

        Team team = Optional.ofNullable(teamRepository.findOne(teamID))
                .orElseThrow(() -> new NoSuchElementException("No such team found"));

        if (!user.getTeams().contains(team)) {
            throw new NoSuchElementException("Such user is not present in this Team");
        }
    }

}
