package com.epam.smartman.service.Impl;

import com.epam.smartman.controller.DTO.IssueDTO;
import com.epam.smartman.domain.*;
import com.epam.smartman.domain.enums.IssueType;
import com.epam.smartman.domain.enums.Priority;
import com.epam.smartman.domain.enums.Status;
import com.epam.smartman.exceptions.NotAuthorisedUserException;
import com.epam.smartman.repository.*;
import com.epam.smartman.service.AccessService;
import com.epam.smartman.service.IssueService;
import com.epam.smartman.service.SprintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class IssueServiceImpl implements IssueService {

    @Autowired
    IssueRepository issueRepository;
    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    SprintRepository sprintRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    EpicRepository epicRepository;
    @Autowired
    VersionRepository versionRepository;
    @Autowired
    AccessService accessService;
    @Autowired
    SprintService sprintService;
    @Autowired
    TaskListRepository taskListRepository;

    @Override
    public List<Issue> getIssuesByProjectId(Project project) {
        return issueRepository.findIssuesByProjectAndStatusIsNotAndSprintId(
                project,
                Status.BACKLOG,
                sprintService.getCurrentSprint(project)
        );
    }

    @Override
    public List<Issue> getProjectBacklog(Project project) {
        return issueRepository.findBacklogByProjectAndSprintIsNull(project);
    }

    @Override
    public Issue getIssue(Long issueId) {
        return Optional.ofNullable(issueRepository.findOne(issueId))
                .orElseThrow(() -> new NoSuchElementException("No such issue found"));
    }

    @Override
    public List<Issue> getSprintBacklog(Project project, Long sprintId) {
        Sprint sprint = Optional.ofNullable(sprintRepository.findOne(sprintId))
                .orElseThrow(() -> new NoSuchElementException("No such sprint found"));
        return issueRepository.findBacklogByProjectAndSprint(project, sprint);
    }

    @Transactional
    @Override
    public Issue createIssue(Project project, IssueDTO newIssue) throws NotAuthorisedUserException {
        User user = accessService.getAuthorizedUser();
        User assignee =
                newIssue.getAssigneeId() != null ? userRepository.findOne(newIssue.getAssigneeId()) : null;
        Epic epic = newIssue.getEpicId() != null ? epicRepository.findOne(newIssue.getEpicId()) : null;
        Version version =
                newIssue.getVersionId() != null ? versionRepository.findOne(newIssue.getVersionId()) : null;
        TaskList taskList =
                newIssue.getTaskListId() != null ? taskListRepository.findOne(newIssue.getTaskListId())
                        : null;

        Issue issue = new Issue();
        issue.setCreated(new Date());
        issue.setDescriprion(newIssue.getDescription());
        issue.setIssueType(IssueType.valueOf(newIssue.getIssueType()));
        issue.setPriority(Priority.valueOf(newIssue.getPriority()));
        issue.setStatus(newIssue.getStatus() == null ? null : Status.valueOf(newIssue.getStatus()));
        issue.setStoryPoints(newIssue.getStoryPoints());
        issue.setSummary(newIssue.getSummary());
        issue.setAssignee(assignee);
        issue.setEpic(epic);
        issue.setProject(project);
        issue.setReporter(user);
        issue.setVersion(version);
        issue.setTaskList(taskList);

        if (newIssue.getSprintId() == null) {
            issue.setSprint(null);
        } else {
            issue.setSprint(sprintRepository.findOne(newIssue.getSprintId()));
        }

        return issueRepository.save(issue);
    }

    @Transactional
    @Override
    public Issue updateIssue(IssueDTO newIssue, Long issueId) {
        Issue issue = Optional.ofNullable(issueRepository.findOne(issueId))
                .orElseThrow(() -> new NoSuchElementException("No such issue found"));
        User assignee =
                newIssue.getAssigneeId() != null ? userRepository.findOne(newIssue.getAssigneeId()) : null;
        Epic epic = newIssue.getEpicId() != null ? epicRepository.findOne(newIssue.getEpicId()) : null;
        Version version =
                newIssue.getVersionId() != null ? versionRepository.findOne(newIssue.getVersionId()) : null;
        TaskList taskList =
                newIssue.getTaskListId() != null ? taskListRepository.findOne(newIssue.getTaskListId())
                        : null;

        issue.setDescriprion(newIssue.getDescription());
        issue.setIssueType(IssueType.valueOf(newIssue.getIssueType()));
        issue.setPriority(Priority.valueOf(newIssue.getPriority()));
        issue.setStatus(Status.valueOf(newIssue.getStatus()));
        issue.setStoryPoints(newIssue.getStoryPoints());
        issue.setSummary(newIssue.getSummary());
        issue.setAssignee(assignee);
        issue.setEpic(epic);
        issue.setVersion(version);
        issue.setUpdated(new Date());
        issue.setTaskList(taskList);

        if (newIssue.getSprintId() == null) {
            issue.setSprint(null);
        } else {
            issue.setSprint(sprintRepository.findOne(newIssue.getSprintId()));
        }

        return issueRepository.save(issue);

    }

    @Transactional
    @Override
    public void deleteIssue(Long issueId) {
        Issue issue = Optional.ofNullable(issueRepository.findOne(issueId))
                .orElseThrow(() -> new NoSuchElementException("No such issue found"));
        issueRepository.delete(issue);
    }

    @Override
    @Transactional
    public Issue updateIssueStatus(Long issueId, String newStatus) {
        Issue issue = Optional.ofNullable(issueRepository.findOne(issueId))
                .orElseThrow(() -> new NoSuchElementException("No such issue found"));

        for (Status status : Status.values()) {
            if (status.toString().equals(newStatus)) {
                issue.setStatus(status);
                break;
            }
        }
        return issueRepository.save(issue);
    }

    @Override
    @Transactional
    public Issue updateIssueTaskList(Long issueId, Long taskListId) {
        Issue issue = Optional.ofNullable(issueRepository.findOne(issueId))
                .orElseThrow(() -> new NoSuchElementException("No such issue found"));
        TaskList taskList = taskListRepository.findOne(taskListId);
        issue.setTaskList(taskList);
        return issueRepository.save(issue);
    }

    @Override
    public List<Issue> getTaskList(Project project, Long taskListId) {
        TaskList taskList = Optional.ofNullable(taskListRepository.findOne(taskListId))
                .orElseThrow(() -> new NoSuchElementException("No such task_list found"));
        return issueRepository.findIssuesByProjectAndTaskListAndSprintId(project, taskList,
                sprintService.getCurrentSprint(project));
    }
}