package com.epam.smartman.service.Impl;

import com.epam.smartman.domain.Project;
import com.epam.smartman.domain.TaskList;
import com.epam.smartman.repository.ProjectRepository;
import com.epam.smartman.repository.TaskListRepository;
import com.epam.smartman.service.TaskListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class TaskListServiceImpl implements TaskListService {

    @Autowired
    TaskListRepository taskListRepository;

    @Autowired
    ProjectRepository projectRepository;

    @Override
    public List<TaskList> getTaskLists(Project project) {
        return taskListRepository.findByProject(project);
    }

    @Transactional
    @Override
    public TaskList addTaskList(Project project, String taskListName) {
        TaskList taskList = new TaskList();
        taskList.setProject(project);
        taskList.setName(taskListName);
        return taskListRepository.save(taskList);
    }

    @Override
    public TaskList changeTaskListName(Long taskListId, String taskListName) {
        TaskList taskList = Optional.ofNullable(taskListRepository.findOne(taskListId))
                .orElseThrow(() -> new NoSuchElementException("No such card list found"));
        taskList.setName(taskListName);
        return taskListRepository.save(taskList);
    }

    @Override
    public void addScrumTaskLists(Project project) {
        addTaskList(project, "TO_DO");
        addTaskList(project, "IN_PROGRESS");
        addTaskList(project, "IN_QA");
        addTaskList(project, "DONE");
    }
}