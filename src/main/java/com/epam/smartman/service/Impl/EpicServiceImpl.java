package com.epam.smartman.service.Impl;

import com.epam.smartman.domain.Epic;
import com.epam.smartman.repository.EpicRepository;
import com.epam.smartman.service.EpicService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;


@Service
public class EpicServiceImpl implements EpicService {

  private EpicRepository epicRepository;

  @Autowired
  public EpicServiceImpl(EpicRepository epicRepository) {
    this.epicRepository = epicRepository;
  }

  @Override
  public List<Epic> getAllEpics() {
    return epicRepository.findAll();
  }

  @Transactional
  @Override
  public Epic createEpic(Epic epic) {
    return epicRepository.save(epic);
  }

  @Override
  public Epic readEpic(Long id) {
    return Optional.ofNullable(epicRepository.findOne(id))
        .orElseThrow(() -> new NoSuchElementException("No such epic found"));//1.5.10
  }

  @Override
  public Epic updateEpic(Long id, Epic epic) {
    Epic epicUpdate = Optional.ofNullable(epicRepository.findOne(id))
        .orElseThrow(() -> new NoSuchElementException("No such epic found"));//1.5.10
    epicUpdate.setName(epic.getName());
    epicUpdate.setSummary(epic.getSummary());
    epicUpdate.setDescription(epic.getDescription());
    epicUpdate.setPriority(epic.getPriority());
    epicUpdate.setAssignee(epic.getAssignee());
    epicUpdate.setReporter(epic.getReporter());
    epicUpdate.setStatus(epic.getStatus());
    epicUpdate.setUpdated(epic.getUpdated());

    return epicRepository.save(epicUpdate);
  }

  @Override
  public void deleteEpic(Long id) {
    Epic epic = Optional.ofNullable(epicRepository.findOne(id))
        .orElseThrow(() -> new NoSuchElementException("No such epic found"));//1.5.10

    epicRepository.delete(epic);
  }
}
