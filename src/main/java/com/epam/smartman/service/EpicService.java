package com.epam.smartman.service;

import com.epam.smartman.domain.Epic;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface EpicService {
    List<Epic> getAllEpics();

    Epic createEpic(Epic epic);

    Epic readEpic(Long id);

    Epic updateEpic(Long id, Epic epic);

    void deleteEpic(Long id);
}
