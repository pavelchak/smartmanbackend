package com.epam.smartman.domain;

import com.epam.smartman.domain.enums.CommentType;

import javax.persistence.*;

@Entity
public class Comment {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Long id;

  @Column(name = "contents", nullable = false, columnDefinition = "MEDIUMTEXT")
  private String contents;

  @Column(name = "edited", nullable = false, columnDefinition = "bit(1) default 0")
  private Boolean edited;

  @ManyToOne
  @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
  private User user;

  @Column(name = "type", nullable = false, length = 15)
  @Enumerated(EnumType.STRING)
  private CommentType type;

  @ManyToOne
  @JoinColumn(name = "issue_id", referencedColumnName = "id")
  private Issue issue;

  @ManyToOne
  @JoinColumn(name = "epic_id", referencedColumnName = "id")
  private Epic epic;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getContents() {
    return contents;
  }

  public void setContents(String contents) {
    this.contents = contents;
  }

  public Boolean getEdited() {
    return edited;
  }

  public void setEdited(Boolean edited) {
    this.edited = edited;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public CommentType getType() {
    return type;
  }

  public void setType(CommentType type) {
    this.type = type;
  }

  public Issue getIssue() {
    return issue;
  }

  public void setIssue(Issue issue) {
    this.issue = issue;
  }

  public Epic getEpic() {
    return epic;
  }

  public void setEpic(Epic epic) {
    this.epic = epic;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Comment comment = (Comment) o;

    if (id != null ? !id.equals(comment.id) : comment.id != null) {
      return false;
    }
    if (contents != null ? !contents.equals(comment.contents) : comment.contents != null) {
      return false;
    }
    if (edited != null ? !edited.equals(comment.edited) : comment.edited != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (contents != null ? contents.hashCode() : 0);
    result = 31 * result + (edited != null ? edited.hashCode() : 0);
    return result;
  }
}
