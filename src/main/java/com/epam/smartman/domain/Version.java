package com.epam.smartman.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Version {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Long id;

  @Column(name = "name", nullable = false, length = 50)
  private String name;

  @Column(name = "description", nullable = true, length = 255)
  private String description;

  @Column(name = "start_date", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date startDate;

  @Column(name = "release_date", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date releaseDate;

  @Column(name = "released", nullable = false, columnDefinition = "bit(1) default 0")
  private Boolean released;

  @ManyToOne
  @JoinColumn(name = "project_id", referencedColumnName = "id", nullable = false)
  private Project project;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Date getStartDate() {
    return new Date(startDate.getTime());
  }

  public void setStartDate(Date startDate) {
    this.startDate = new Date();
    this.startDate.setTime(startDate.getTime());
  }

  public Date getReleaseDate() {
    return releaseDate == null ? null : new Date(releaseDate.getTime());
  }

  public void setReleaseDate(Date releaseDate) {
    this.releaseDate = new Date();
    this.releaseDate.setTime(releaseDate.getTime());
  }

  public Boolean getReleased() {
    return released;
  }

  public void setReleased(Boolean released) {
    this.released = released;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Version version = (Version) o;

    if (id != null ? !id.equals(version.id) : version.id != null) {
      return false;
    }
    if (name != null ? !name.equals(version.name) : version.name != null) {
      return false;
    }
    if (description != null ? !description.equals(version.description)
        : version.description != null) {
      return false;
    }
    if (startDate != null ? !startDate.equals(version.startDate) : version.startDate != null) {
      return false;
    }
    if (releaseDate != null ? !releaseDate.equals(version.releaseDate)
        : version.releaseDate != null) {
      return false;
    }
    if (released != null ? !released.equals(version.released) : version.released != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
    result = 31 * result + (releaseDate != null ? releaseDate.hashCode() : 0);
    result = 31 * result + (released != null ? released.hashCode() : 0);
    return result;
  }
}
