package com.epam.smartman.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class TaskList implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @ManyToOne
    @JoinColumn(name = "project_id", referencedColumnName = "id", nullable = false)
    private Project project;

    public TaskList() {
    }

    public TaskList(String name, Project project) {
        this.name = name;
        this.project = project;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskList)) return false;
        TaskList taskList = (TaskList) o;
        return Objects.equals(getId(), taskList.getId()) &&
                Objects.equals(getName(), taskList.getName()) &&
                Objects.equals(getProject(), taskList.getProject());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getName(), getProject());
    }
}