package com.epam.smartman.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
public class Team implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Long id;

  @Column(name = "name", nullable = false, length = 50)
  private String name;

  @ManyToMany
  @JoinTable(name = "team_project", catalog = "", schema = "smartman",
      joinColumns = @JoinColumn(name = "team_id", referencedColumnName = "id", nullable = false),
      inverseJoinColumns = @JoinColumn(name = "project_id", referencedColumnName = "id", nullable = false))
  private Set<Project> projects;

  @ManyToMany
  @JoinTable(name = "team_user", catalog = "", schema = "smartman",
      joinColumns = @JoinColumn(name = "team_id", referencedColumnName = "id", nullable = false),
      inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false))
  private Set<User> users;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<Project> getProjects() {
    return projects;
  }

  public void setProjects(Set<Project> projects) {
    this.projects = projects;
  }

  public Set<User> getUsers() {
    return users;
  }

  public void setUsers(Set<User> users) {
    this.users = users;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Team team = (Team) o;

    if (id != null ? !id.equals(team.id) : team.id != null) {
      return false;
    }
    if (name != null ? !name.equals(team.name) : team.name != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    return result;
  }
}
