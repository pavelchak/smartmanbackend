package com.epam.smartman.domain;

import com.epam.smartman.domain.enums.IssueType;
import com.epam.smartman.domain.enums.Priority;
import com.epam.smartman.domain.enums.Status;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Issue {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Long id;

  @Column(name = "summary", nullable = false, length = 255)
  private String summary;

  @Column(name = "created", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date created;

  @Column(name = "updated", nullable = true)
  @Temporal(TemporalType.TIMESTAMP)
  private Date updated;

  @Column(name = "description", nullable = true, columnDefinition = "MEDIUMTEXT")
  private String description;

  @Column(name = "story_points", nullable = true)
  private Integer storyPoints;

  @Column(name = "status", nullable = true, length = 15)
  @Enumerated(EnumType.STRING)
  private Status status;

  @ManyToOne
  @JoinColumn(name = "reporter_id", referencedColumnName = "id", nullable = true)
  private User reporter;

  @ManyToOne
  @JoinColumn(name = "assignee_id", referencedColumnName = "id")
  private User assignee;

  @Column(name = "priority", nullable = false, length = 10)
  @Enumerated(EnumType.STRING)
  private Priority priority;

  @ManyToOne
  @JoinColumn(name = "sprint_id", referencedColumnName = "id")
  private Sprint sprint;

  @ManyToOne
  @JoinColumn(name = "project_id", referencedColumnName = "id", nullable = false)
  private Project project;

  @Column(name = "type", nullable = false, length = 10)
  @Enumerated(EnumType.STRING)
  private IssueType issueType;

  @ManyToOne
  @JoinColumn(name = "epic_id", referencedColumnName = "id")
  private Epic epic;

  @ManyToOne
  @JoinColumn(name = "version_id", referencedColumnName = "id")
  private Version version;

  @ManyToOne
  @JoinColumn(name = "task_list_id", referencedColumnName = "id")
  private TaskList taskList;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public void setCreated(Date created) {
    this.created = new Date();
    this.created.setTime(created.getTime());
  }

  public Date getCreated() {
    return created == null ? null : new Date(created.getTime());
  }

  public Date getUpdated() {
    return updated == null ? null : new Date(updated.getTime());
  }

  public void setUpdated(Date updated) {
    this.updated = new Date();
    this.updated.setTime(updated.getTime());
  }

  public String getDescription() {
    return description;
  }

  public void setDescriprion(String description) {
    this.description = description;
  }

  public Integer getStoryPoints() {
    return storyPoints;
  }

  public void setStoryPoints(Integer storyPoints) {
    this.storyPoints = storyPoints;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public User getReporter() {
    return reporter;
  }

  public void setReporter(User reporter) {
    this.reporter = reporter;
  }

  public User getAssignee() {
    return assignee;
  }

  public void setAssignee(User assignee) {
    this.assignee = assignee;
  }

  public Priority getPriority() {
    return priority;
  }

  public void setPriority(Priority priority) {
    this.priority = priority;
  }

  public Sprint getSprint() {
    return sprint;
  }

  public void setSprint(Sprint sprint) {
    this.sprint = sprint;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  public IssueType getIssueType() {
    return issueType;
  }

  public void setIssueType(IssueType issueType) {
    this.issueType = issueType;
  }

  public Epic getEpic() {
    return epic;
  }

  public void setEpic(Epic epic) {
    this.epic = epic;
  }

  public Version getVersion() {
    return version;
  }

  public void setVersion(Version version) {
    this.version = version;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public TaskList getTaskList() {
    return taskList;
  }

  public void setTaskList(TaskList taskList) {
    this.taskList = taskList;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Issue issue = (Issue) o;

    if (id != null ? !id.equals(issue.id) : issue.id != null) {
      return false;
    }
    if (summary != null ? !summary.equals(issue.summary) : issue.summary != null) {
      return false;
    }
    if (created != null ? !created.equals(issue.created) : issue.created != null) {
      return false;
    }
    if (updated != null ? !updated.equals(issue.updated) : issue.updated != null) {
      return false;
    }
    if (description != null ? !description.equals(issue.description) : issue.description != null) {
      return false;
    }
    if (storyPoints != null ? !storyPoints.equals(issue.storyPoints) : issue.storyPoints != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (summary != null ? summary.hashCode() : 0);
    result = 31 * result + (created != null ? created.hashCode() : 0);
    result = 31 * result + (updated != null ? updated.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (storyPoints != null ? storyPoints.hashCode() : 0);
    return result;
  }
}