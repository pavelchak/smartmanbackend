package com.epam.smartman.domain;

import com.epam.smartman.domain.enums.ProjectType;

import javax.persistence.*;

import java.io.Serializable;

import java.util.List;

import java.util.Set;

@Entity
public class Project implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @Column(name = "short_name", nullable = true, length = 10)
    private String shortName;

    @Column(name = "description", nullable = true, columnDefinition = "MEDIUMTEXT")
    private String description;

    @Column(name = "active", nullable = false)
    private Boolean active;

    @Column(name = "project_type", nullable = true, length = 10)
    @Enumerated(EnumType.STRING)
    private ProjectType projectType;

    @ManyToOne
    @JoinColumn(name = "project_creator_id", referencedColumnName = "id", nullable = true)
    private  User projectCreator;

    @ManyToMany(mappedBy = "projects")
    private Set<Team> teams;

    @OneToMany(mappedBy = "project", fetch = FetchType.EAGER)
    private List<ProjectUser> projectUsers;

    @OneToMany(mappedBy = "project")
    private List<TaskList> taskLists;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getProjectCreator() {
        return projectCreator;
    }

    public void setProjectCreator(User projectCreator) {
        this.projectCreator = projectCreator;
    }

    public Set<Team> getTeams() {
        return teams;
    }

    public void setTeams(Set<Team> teams) {
        this.teams = teams;
    }

    public void removeTeam(Team team) {
        teams.remove(team);
    }

    public void addTeam(Team team) {
        teams.add(team);
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public List<ProjectUser> getProjectUsers() {
        return projectUsers;
    }

    public void setProjectUsers(List<ProjectUser> projectUsers) {
        this.projectUsers = projectUsers;
    }

    public List<TaskList> getTaskLists() {
        return taskLists;
    }

    public void setTaskLists(List<TaskList> taskLists) {
        this.taskLists = taskLists;
    }

    public ProjectType getProjectType() {
        return projectType;
    }

    public void setProjectType(ProjectType projectType) {
        this.projectType = projectType;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Project project = (Project) o;

        if (id != null ? !id.equals(project.id) : project.id != null) {
            return false;
        }
        if (name != null ? !name.equals(project.name) : project.name != null) {
            return false;
        }
        if (shortName != null ? !shortName.equals(project.shortName) : project.shortName != null) {
            return false;
        }
        if (description != null ? !description.equals(project.description)
                : project.description != null) {
            return false;
        }
        if (active != null ? !active.equals(project.active) : project.active != null) {
            return false;
        }
        if (projectCreator != null ? !projectCreator.equals(project.projectCreator)
                : project.projectCreator != null) {
            return false;
        }
        if (teams != null ? !teams.equals(project.teams) : project.teams != null) {
            return false;
        }
        if (projectUsers != null ? !projectUsers.equals(project.projectUsers)
                : project.projectUsers != null) {
            return false;
        }
        return taskLists != null ? taskLists.equals(project.taskLists) : project.taskLists == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (shortName != null ? shortName.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (active != null ? active.hashCode() : 0);
        result = 31 * result + (projectCreator != null ? projectCreator.hashCode() : 0);
        result = 31 * result + (teams != null ? teams.hashCode() : 0);
        result = 31 * result + (projectUsers != null ? projectUsers.hashCode() : 0);
        result = 31 * result + (taskLists != null ? taskLists.hashCode() : 0);
        return result;
    }
}