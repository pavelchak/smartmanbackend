package com.epam.smartman.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class ProjectUserPK implements Serializable {

  @Id
  @Column(name = "project_id", nullable = false)
  private Long projectId;

  @Id
  @Column(name = "user_id", nullable = false)
  private Long userId;

  public Long getProjectId() {
    return projectId;
  }

  public void setProjectId(Long projectId) {
    this.projectId = projectId;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    ProjectUserPK that = (ProjectUserPK) o;

    if (projectId != null ? !projectId.equals(that.projectId) : that.projectId != null) {
      return false;
    }
    if (userId != null ? !userId.equals(that.userId) : that.userId != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = projectId != null ? projectId.hashCode() : 0;
    result = 31 * result + (userId != null ? userId.hashCode() : 0);
    return result;
  }
}
