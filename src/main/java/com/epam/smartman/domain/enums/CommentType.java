package com.epam.smartman.domain.enums;

public enum CommentType {
    COMMENT,
    HISTORY;
}
