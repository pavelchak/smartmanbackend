package com.epam.smartman.domain.enums;

public enum Authority {
  ROLE_USER, ROLE_ADMIN;
}
