package com.epam.smartman.domain.enums;

public enum IssueType {
  STORY,
  BUG;
}
