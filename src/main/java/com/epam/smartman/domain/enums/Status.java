package com.epam.smartman.domain.enums;

public enum Status {
    BACKLOG,
    TO_DO,
    IN_PROGRESS,
    IN_QA,
    DONE
}
