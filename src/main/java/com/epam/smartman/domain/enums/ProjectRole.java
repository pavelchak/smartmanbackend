package com.epam.smartman.domain.enums;

public enum ProjectRole {
  CREATOR,
  READER,
  WRITER,
}
