package com.epam.smartman.domain.enums;

public enum ProjectType {
    SCRUM,
    KANBAN;
}