package com.epam.smartman.domain;

import com.epam.smartman.domain.enums.Priority;
import com.epam.smartman.domain.enums.Status;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Epic {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Long id;

  @Column(name = "name", nullable = false, length = 50)
  private String name;

  @Column(name = "summary", nullable = false, length = 255)
  private String summary;

  @Column(name = "created", nullable = false)
//  private Timestamp created;
  @Temporal(TemporalType.TIMESTAMP)
  private Date created;

  @Column(name = "updated", nullable = true)
//  private Timestamp updated;
  @Temporal(TemporalType.TIMESTAMP)
  private Date updated;

  @Column(name = "description", nullable = true, columnDefinition = "MEDIUMTEXT")
  private String description;

  @Column(name = "status", nullable = false, length = 15)
  @Enumerated(EnumType.STRING)
  private Status status;

  @ManyToOne
  @JoinColumn(name = "reporter_id", referencedColumnName = "id", nullable = false)
  private User reporter;

  @ManyToOne
  @JoinColumn(name = "assignee_id", referencedColumnName = "id")
  private User assignee;

  @Column(name = "priority", nullable = false, length = 10)
  @Enumerated(EnumType.STRING)
  private Priority priority;

  @ManyToOne
  @JoinColumn(name = "project_id", referencedColumnName = "id", nullable = false)
  private Project project;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public Date getCreated() {
    return new Date(created.getTime());
  }

  public void setCreated(Date created) {
    this.created = new Date();
    this.created.setTime(created.getTime());
  }

  public Date getUpdated() {
    return updated == null ? null : new Date(updated.getTime());
  }

  public void setUpdated(Date updated) {
    this.updated = new Date();
    this.updated.setTime(updated.getTime());
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public User getReporter() {
    return reporter;
  }

  public void setReporter(User reporter) {
    this.reporter = reporter;
  }

  public User getAssignee() {
    return assignee;
  }

  public void setAssignee(User assignee) {
    this.assignee = assignee;
  }

  public Priority getPriority() {
    return priority;
  }

  public void setPriority(Priority priority) {
    this.priority = priority;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Epic epic = (Epic) o;

    if (id != null ? !id.equals(epic.id) : epic.id != null) {
      return false;
    }
    if (name != null ? !name.equals(epic.name) : epic.name != null) {
      return false;
    }
    if (summary != null ? !summary.equals(epic.summary) : epic.summary != null) {
      return false;
    }
    if (created != null ? !created.equals(epic.created) : epic.created != null) {
      return false;
    }
    if (updated != null ? !updated.equals(epic.updated) : epic.updated != null) {
      return false;
    }
    if (description != null ? !description.equals(epic.description) : epic.description != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (summary != null ? summary.hashCode() : 0);
    result = 31 * result + (created != null ? created.hashCode() : 0);
    result = 31 * result + (updated != null ? updated.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    return result;
  }
}
