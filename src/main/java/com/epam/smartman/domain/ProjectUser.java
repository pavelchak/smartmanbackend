package com.epam.smartman.domain;

import com.epam.smartman.domain.enums.ProjectRole;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "project_user", schema = "smartman", catalog = "")
@IdClass(ProjectUserPK.class)
public class ProjectUser implements Serializable {

  @Id
  @Column(name = "project_id", nullable = false)
  private Long projectId;

  @Id
  @Column(name = "user_id", nullable = false)
  private Long userId;

  @ManyToOne
  @JoinColumn(name = "project_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
  private Project project;

  @ManyToOne
  @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
  private User user;

  @Column(name = "role", nullable = false, length = 20)
  @Enumerated(EnumType.STRING)
  private ProjectRole projectRole;

  public Long getProjectId() {
    return projectId;
  }

  public void setProjectId(Long projectId) {
    this.projectId = projectId;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public ProjectRole getProjectRole() {
    return projectRole;
  }

  public void setProjectRole(ProjectRole projectRole) {
    this.projectRole = projectRole;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof ProjectUser)) return false;
    ProjectUser that = (ProjectUser) o;
    return Objects.equals(getProjectId(), that.getProjectId()) &&
            Objects.equals(getUserId(), that.getUserId()) &&
            Objects.equals(getProject(), that.getProject()) &&
            Objects.equals(getUser(), that.getUser()) &&
            getProjectRole() == that.getProjectRole();
  }

  @Override
  public int hashCode() {
    return Objects.hash(getProjectId(), getUserId(), getProject(), getUser(), getProjectRole());
  }
}
