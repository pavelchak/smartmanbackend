package com.epam.smartman.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Sprint {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @Column(name = "start_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Column(name = "deadline", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date deadline;

    @Column(name = "close_date", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date closeDate;

    @Column(name = "description", nullable = true, columnDefinition = "MEDIUMTEXT")
    private String description;

    @ManyToOne
    @JoinColumn(name = "project_id", referencedColumnName = "id", nullable = false)
    private Project project;

    @ManyToOne
    @JoinColumn(name = "version_id", referencedColumnName = "id")
    private Version version;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return new Date(startDate.getTime());
    }

    public void setStartDate(Date startDate) {
        this.startDate = new Date();
        this.startDate.setTime(startDate.getTime());
    }

    public Date getDeadline() {
        return new Date(deadline.getTime());
    }

    public void setDeadline(Date deadline) {
        this.deadline = new Date();
        this.deadline.setTime(deadline.getTime());
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public Date getCloseDate() {
        return closeDate == null ? null : new Date(closeDate.getTime());
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = new Date();
        this.closeDate.setTime(closeDate.getTime());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Sprint sprint = (Sprint) o;

        if (id != null ? !id.equals(sprint.id) : sprint.id != null) {
            return false;
        }
        if (name != null ? !name.equals(sprint.name) : sprint.name != null) {
            return false;
        }
        if (startDate != null ? !startDate.equals(sprint.startDate) : sprint.startDate != null) {
            return false;
        }
        if (deadline != null ? !deadline.equals(sprint.deadline) : sprint.deadline != null) {
            return false;
        }
        if (closeDate != null ? !closeDate.equals(sprint.closeDate) : sprint.closeDate != null) {
            return false;
        }
        if (description != null ? !description.equals(sprint.description)
                : sprint.description != null) {
            return false;
        }
        if (project != null ? !project.equals(sprint.project) : sprint.project != null) {
            return false;
        }
        return version != null ? version.equals(sprint.version) : sprint.version == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (deadline != null ? deadline.hashCode() : 0);
        result = 31 * result + (closeDate != null ? closeDate.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (project != null ? project.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        return result;
    }
}
