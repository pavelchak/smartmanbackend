package com.epam.smartman.domain;

import com.epam.smartman.domain.enums.Authority;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity

public class User  implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "surname", nullable = false, length = 45)
    private String surname;
    @Column(name = "name", nullable = false, length = 45)
    private String username;
    @Column(name = "password", nullable = false, length = 200)
    private String password;
    @Column(name = "email", nullable = false, length = 45)
    private String email;

    @ManyToMany(mappedBy = "users")
    private Set<Team> teams;
    @Enumerated(EnumType.STRING)
    private Authority authority = Authority.ROLE_USER;


    public User() {
    }

    public Long getId() {
        return id;
    }

    public User setId(Long id) {
        this.id = id;
        return this;
    }

    public User setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public User setUsername(String username) {
        this.username = username;
        return this;
    }

  public User setPassword(String password) {
    this.password = password;
    return this;
  }

    public String getPassword() {
        return password;
    }

    public Authority getAuthority() {
        return authority;
    }

    public User setAuthority(Authority authority) {
        this.authority = authority;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public Set<Team> getTeams() {
        return teams;
    }

    public User setTeams(Set<Team> teams) {
        this.teams = teams;
        return this;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", teams=" + teams +
                ", authority=" + authority +
                '}';
    }
}
