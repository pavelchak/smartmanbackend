package com.epam.smartman.repository;

import com.epam.smartman.domain.Issue;
import com.epam.smartman.domain.Project;
import com.epam.smartman.domain.Sprint;
import com.epam.smartman.domain.TaskList;
import com.epam.smartman.domain.enums.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IssueRepository extends JpaRepository<Issue, Long> {

    List<Issue> findBacklogByProjectAndSprintIsNull(Project project);

    List<Issue> findBacklogByProjectAndSprint(Project project, Sprint sprint);

    List<Issue> findIssuesByProjectAndStatusIsNotAndSprintId(Project project, Status status,
                                                             Long sprintId);

    List<Issue> findIssuesByProjectAndTaskListAndSprintId(Project project, TaskList taskList, Long sprintId);
}