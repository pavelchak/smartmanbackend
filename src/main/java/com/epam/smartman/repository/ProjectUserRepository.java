package com.epam.smartman.repository;

import com.epam.smartman.domain.Project;
import com.epam.smartman.domain.ProjectUser;
import com.epam.smartman.domain.ProjectUserPK;
import com.epam.smartman.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectUserRepository extends JpaRepository<ProjectUser, ProjectUserPK> {

  ProjectUser findByProjectAndUser(Project project, User user);

  List<ProjectUser> findUsersByProject(Project project);

}
