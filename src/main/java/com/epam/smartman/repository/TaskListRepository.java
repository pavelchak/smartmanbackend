package com.epam.smartman.repository;

import com.epam.smartman.domain.Project;
import com.epam.smartman.domain.TaskList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TaskListRepository  extends JpaRepository<TaskList, Long> {
    @Query("SELECT t FROM TaskList t WHERE t.project=?1 ORDER BY t.id")
    List<TaskList> findByProject(Project project);
}