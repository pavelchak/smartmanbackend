package com.epam.smartman.repository;

import com.epam.smartman.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

   User findUserByEmail(@Param("email") String email);


  User findUserByUsername(@Param("username") String username);

  @Query("SELECT u FROM User u WHERE CONCAT(u.surname, ' ', u.username) LIKE %?1%")
  List<User> findUsersBySurnameAndNameLike(String pattern);

}
