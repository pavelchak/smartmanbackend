package com.epam.smartman.repository;

import com.epam.smartman.domain.Project;
import com.epam.smartman.domain.Sprint;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SprintRepository extends JpaRepository<Sprint, Long> {

  List<Sprint> findSprintsByProject(Project project);

  Sprint findByProjectAndCloseDateNull(Project project);
}
