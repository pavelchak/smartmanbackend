package com.epam.smartman.repository;

import com.epam.smartman.domain.Project;
import com.epam.smartman.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

    @Query("SELECT distinct p FROM Project p LEFT JOIN p.projectUsers p2 WHERE (p2.user=?1 OR p.projectCreator=?1) AND Active=TRUE")
    List<Project> findProjectsByUser(User user);

}
