package com.epam.smartman.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@Configuration
public class ScriptLoaderConfiguration {

  @Value("${loadTestDB}")
  boolean scriptLoader;

  @Autowired
  @Qualifier("dataSource")
  private DataSource dataSource;

  @PostConstruct
  public void loadByCondition() throws SQLException {
    if (scriptLoader) {
      loadSqlScript();
    }
  }

  public void loadSqlScript() throws SQLException {
    ClassPathResource resource = new ClassPathResource("data/testdata.sql");
    Connection con = dataSource.getConnection();
    ScriptUtils.executeSqlScript(con, resource);
  }
}
