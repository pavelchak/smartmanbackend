package com.epam.smartman.config;

import com.epam.smartman.domain.enums.Authority;
import com.epam.smartman.service.UserService;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

@Component
public class AdminPagesFilter extends GenericFilterBean {

  private static final List<String> PAGES_TO_PROTECT = Lists
      .newArrayList("/admin-page.html", "/adminTeamPage.html");
  private static final String DEFAULT_URI = "/";


  @Autowired
  private UserService userService;

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
      FilterChain filterChain) throws IOException, ServletException {
    String uri = ((HttpServletRequest) servletRequest).getRequestURI();
    if (PAGES_TO_PROTECT.stream().anyMatch(uri::equalsIgnoreCase)) {
      try {
        Authentication authentication = TokenAuthenticationService
            .getAuthentication((HttpServletRequest) servletRequest);
        String email = (String) Optional.ofNullable(authentication)
            .orElseThrow(NullPointerException::new).getPrincipal();
        if (!userService.findByEmail(email).getAuthority().equals(Authority.ROLE_ADMIN)) {
          redirectToDefault((HttpServletResponse) servletResponse);
        }
      } catch (NullPointerException e) {
        redirectToDefault((HttpServletResponse) servletResponse);
      }
    }
    filterChain.doFilter(servletRequest, servletResponse);
  }

  private void redirectToDefault(HttpServletResponse servletResponse) throws IOException {
    if (!servletResponse.isCommitted()) {
      servletResponse.sendRedirect(DEFAULT_URI);
    }
  }
}
