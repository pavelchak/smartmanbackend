package com.epam.smartman.utils;

public class StringUtils {

    public static String fromListNameToStatusName(String listName){
        switch (listName){
            case "ToDo":
                return "TO_DO";
            case "InProgress":
                return "IN_PROGRESS";
            case "InQA":
                return "IN_QA";
            case "Done":
                return "DONE";
            default:
                return "BACKLOG";
        }
    }
}
