package com.epam.smartman.controller;

import com.epam.smartman.controller.DTO.IssueDTO;
import com.epam.smartman.controller.DTO.convertor.IssueDTOConverter;
import com.epam.smartman.domain.Issue;
import com.epam.smartman.domain.Project;
import com.epam.smartman.domain.enums.ProjectRole;
import com.epam.smartman.exceptions.NoAccessRightsException;
import com.epam.smartman.exceptions.NotAuthorisedUserException;
import com.epam.smartman.service.AccessService;
import com.epam.smartman.service.IssueService;
import com.epam.smartman.service.MailSendService;
import com.epam.smartman.service.ProjectService;
import com.epam.smartman.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class IssueController {

    @Autowired
    IssueService issueService;
    @Autowired
    ProjectService projectService;
    @Autowired
    AccessService accessService;
    @Autowired
    MailSendService mailSendService;

    @GetMapping(value = "/api/project/{projectId}/dashboard")
    public ResponseEntity<List<IssueDTO>> getIssuesByProjectId(@PathVariable Long projectId)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.READER);
        List<Issue> issues = issueService.getIssuesByProjectId(project);
        List<IssueDTO> issueDTOs = new ArrayList<>();
        for (Issue issue : issues) {
            IssueDTO issueDTO = IssueDTOConverter.buildIssueDTO(issue);
            issueDTOs.add(issueDTO);
        }
        return new ResponseEntity<>(issueDTOs, HttpStatus.OK);
    }

    @GetMapping(value = "/api/project/{projectId}/backlog")
    public ResponseEntity<List<IssueDTO>> getProjectBacklog(@PathVariable Long projectId)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.READER);
        List<Issue> issueList = issueService.getProjectBacklog(project);
        List<IssueDTO> issueDTOList = new ArrayList<>();
        for (Issue entity : issueList) {
            IssueDTO dto = IssueDTOConverter.buildIssueDTO(entity);
            issueDTOList.add(dto);
        }
        return new ResponseEntity<>(issueDTOList, HttpStatus.OK);
    }

    @GetMapping(value = "/api/project/{projectId}/issue/{issueId}")
    public ResponseEntity<IssueDTO> getIssue(@PathVariable Long projectId,
                                             @PathVariable Long issueId) throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.READER);
        Issue issue = issueService.getIssue(issueId);
        IssueDTO issueDTO = IssueDTOConverter.buildIssueDTO(issue);
        return new ResponseEntity<>(issueDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/api/project/{projectId}/sprint/{sprintId}/issue")
    public ResponseEntity<List<IssueDTO>> getBacklogSprint(@PathVariable Long projectId,
                                                           @PathVariable Long sprintId) throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.READER);
        List<Issue> issueList = issueService.getSprintBacklog(project, sprintId);
        List<IssueDTO> issueDTOList = new ArrayList<>();
        for (Issue entity : issueList) {
            IssueDTO dto = IssueDTOConverter.buildIssueDTO(entity);
            issueDTOList.add(dto);
        }
        return new ResponseEntity<>(issueDTOList, HttpStatus.OK);
    }

    @GetMapping(value = "/api/project/{projectId}/sprint/{sprintId}/issue/{issueId}")
    public ResponseEntity<IssueDTO> getSprintIssue(@PathVariable Long projectId,
                                                   @PathVariable Long sprintId, @PathVariable Long issueId)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.READER);
        Issue issue = issueService.getIssue(issueId);
        IssueDTO issueDTO = IssueDTOConverter.buildIssueDTO(issue);
        return new ResponseEntity<>(issueDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/api/project/{projectId}/issue")
    public ResponseEntity<IssueDTO> addIssue(@PathVariable Long projectId,
                                             @RequestBody IssueDTO newIssue) throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.WRITER);
        Issue issue = issueService.createIssue(project, newIssue);
        IssueDTO issueDTO = IssueDTOConverter.buildIssueDTO(issue);
        return new ResponseEntity<>(issueDTO, HttpStatus.CREATED);
    }

    @PostMapping(value = "/api/project/{projectId}/issue/{issueId}")
    public ResponseEntity updateIssue(@PathVariable Long projectId,
                                      @PathVariable Long issueId, @RequestBody IssueDTO updIssue)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.WRITER);
        Issue issue = issueService.updateIssue(updIssue, issueId);
        
        IssueDTOConverter.buildIssueDTO(issue);
        mailSendService.sendNotification(issue);

        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping(value = "/api/project/{projectId}/issue/{issueId}")
    public ResponseEntity deleteIssue(@PathVariable Long projectId, @PathVariable Long issueId)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.WRITER);
        issueService.deleteIssue(issueId);

        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping(value = "/api/project/{projectId}/issue/{issueId}/{status}")
    public ResponseEntity<IssueDTO> updateIssueStatus(@PathVariable Long projectId,
                                                      @PathVariable Long issueId, @PathVariable String status)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.WRITER);
        Issue issue = issueService
                .updateIssueStatus(issueId, StringUtils.fromListNameToStatusName(status));
        IssueDTO issueDTO = IssueDTOConverter.buildIssueDTO(issue);
        return new ResponseEntity<>(issueDTO, HttpStatus.OK);
    }

    @PutMapping(value = "/api/project/{projectId}/issue/{issueId}/card-list/{taskListID}")
    public ResponseEntity<IssueDTO> updateIssueTaskList(@PathVariable Long projectId,
                                                        @PathVariable Long issueId, @PathVariable Long taskListID)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.WRITER);
        Issue issue = issueService.updateIssueTaskList(issueId, taskListID);
        IssueDTO issueDTO = IssueDTOConverter.buildIssueDTO(issue);
        return new ResponseEntity<>(issueDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/api/project/{projectId}/card-list/{taskListId}/card")
    public ResponseEntity<List<IssueDTO>> getTaskListIssues(@PathVariable Long projectId,
                                                           @PathVariable Long taskListId) throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.READER);
        List<Issue> issueList = issueService.getTaskList(project, taskListId);
        List<IssueDTO> issueDTOList = new ArrayList<>();
        for (Issue entity : issueList) {
            IssueDTO dto = IssueDTOConverter.buildIssueDTO(entity);
            issueDTOList.add(dto);
        }
        return new ResponseEntity<>(issueDTOList, HttpStatus.OK);
    }

    @PutMapping(value = "api/project/{projectId}/card-list/{cardListId}/card/{cardId}")
    public ResponseEntity changeCardList(
            @PathVariable Long projectId, @PathVariable Long cardListId, @PathVariable Long cardId)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.READER);
        issueService.updateIssueTaskList(cardId, cardListId);
        return new ResponseEntity<> (HttpStatus.OK);
    }

}