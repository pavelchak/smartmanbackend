package com.epam.smartman.controller.DTO;

public class IssueDTO {

  private Long issueId;

  private String summary;

  private String description;

  private Integer storyPoints;

  private Long reporterId;

  private Long assigneeId;

  private String assignee;

  private String priority;

  private String issueType;

  private Long epicId;

  private Long versionId;

  private String status;

  private String updated;

  private Long projectId;

  private Long sprintId;

  private String created;

  private Long taskListId;

  public String getAssignee() {
    return assignee;
  }

  public void setAssignee(String assignee) {
    this.assignee = assignee;
  }

  public Long getIssueId() {
    return issueId;
  }

  public void setIssueId(Long issueId) {
    this.issueId = issueId;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Integer getStoryPoints() {
    return storyPoints;
  }

  public void setStoryPoints(Integer storyPoints) {
    this.storyPoints = storyPoints;
  }

  public Long getReporterId() {
    return reporterId;
  }

  public void setReporterId(Long reporterId) {
    this.reporterId = reporterId;
  }

  public Long getAssigneeId() {
    return assigneeId;
  }

  public void setAssigneeId(Long assigneeId) {
    this.assigneeId = assigneeId;
  }

  public String getPriority() {
    return priority;
  }

  public void setPriority(String priority) {
    this.priority = priority;
  }

  public String getIssueType() {
    return issueType;
  }

  public void setIssueType(String issueType) {
    this.issueType = issueType;
  }

  public Long getEpicId() {
    return epicId;
  }

  public void setEpicId(Long epicId) {
    this.epicId = epicId;
  }

  public Long getVersionId() {
    return versionId;
  }

  public void setVersionId(Long versionId) {
    this.versionId = versionId;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getUpdated() {
    return updated;
  }

  public void setUpdated(String updated) {
    this.updated = updated;
  }

  public Long getProjectId() {
    return projectId;
  }

  public void setProjectId(Long projectId) {
    this.projectId = projectId;
  }

  public Long getSprintId() {
    return sprintId;
  }

  public void setSprintId(Long sprintId) {
    this.sprintId = sprintId;
  }

  public String getCreated() {
    return created;
  }

  public void setCreated(String created) {
    this.created = created;
  }

  public Long getTaskListId() {
    return taskListId;
  }

  public void setTaskListId(Long taskListId) {
    this.taskListId = taskListId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    IssueDTO issueDTO = (IssueDTO) o;

    if (issueId != null ? !issueId.equals(issueDTO.issueId) : issueDTO.issueId != null) {
      return false;
    }
    if (summary != null ? !summary.equals(issueDTO.summary) : issueDTO.summary != null) {
      return false;
    }
    if (description != null ? !description.equals(issueDTO.description)
            : issueDTO.description != null) {
      return false;
    }
    if (storyPoints != null ? !storyPoints.equals(issueDTO.storyPoints)
            : issueDTO.storyPoints != null) {
      return false;
    }
    if (reporterId != null ? !reporterId.equals(issueDTO.reporterId)
            : issueDTO.reporterId != null) {
      return false;
    }
    if (assigneeId != null ? !assigneeId.equals(issueDTO.assigneeId)
            : issueDTO.assigneeId != null) {
      return false;
    }
    if (assignee != null ? !assignee.equals(issueDTO.assignee) : issueDTO.assignee != null) {
      return false;
    }
    if (priority != null ? !priority.equals(issueDTO.priority) : issueDTO.priority != null) {
      return false;
    }
    if (issueType != null ? !issueType.equals(issueDTO.issueType) : issueDTO.issueType != null) {
      return false;
    }
    if (epicId != null ? !epicId.equals(issueDTO.epicId) : issueDTO.epicId != null) {
      return false;
    }
    if (versionId != null ? !versionId.equals(issueDTO.versionId) : issueDTO.versionId != null) {
      return false;
    }
    if (status != null ? !status.equals(issueDTO.status) : issueDTO.status != null) {
      return false;
    }
    if (updated != null ? !updated.equals(issueDTO.updated) : issueDTO.updated != null) {
      return false;
    }
    if (projectId != null ? !projectId.equals(issueDTO.projectId) : issueDTO.projectId != null) {
      return false;
    }
    if (sprintId != null ? !sprintId.equals(issueDTO.sprintId) : issueDTO.sprintId != null) {
      return false;
    }
    return created != null ? created.equals(issueDTO.created) : issueDTO.created == null;
  }

  @Override
  public int hashCode() {
    int result = issueId != null ? issueId.hashCode() : 0;
    result = 31 * result + (summary != null ? summary.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (storyPoints != null ? storyPoints.hashCode() : 0);
    result = 31 * result + (reporterId != null ? reporterId.hashCode() : 0);
    result = 31 * result + (assigneeId != null ? assigneeId.hashCode() : 0);
    result = 31 * result + (assignee != null ? assignee.hashCode() : 0);
    result = 31 * result + (priority != null ? priority.hashCode() : 0);
    result = 31 * result + (issueType != null ? issueType.hashCode() : 0);
    result = 31 * result + (epicId != null ? epicId.hashCode() : 0);
    result = 31 * result + (versionId != null ? versionId.hashCode() : 0);
    result = 31 * result + (status != null ? status.hashCode() : 0);
    result = 31 * result + (updated != null ? updated.hashCode() : 0);
    result = 31 * result + (projectId != null ? projectId.hashCode() : 0);
    result = 31 * result + (sprintId != null ? sprintId.hashCode() : 0);
    result = 31 * result + (created != null ? created.hashCode() : 0);
    return result;
  }
}