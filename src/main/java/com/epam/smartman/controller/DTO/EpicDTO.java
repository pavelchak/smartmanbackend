package com.epam.smartman.controller.DTO;

import com.epam.smartman.domain.Epic;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import java.util.Date;
import java.util.Objects;

public class EpicDTO extends ResourceSupport {
    private Epic epic;

    public EpicDTO(Epic epic, Link selfLink){
        this.epic = epic;
        add(selfLink);
    }

    public Long getEpicId(){ return epic.getId();}

    public String getName(){return epic.getName();}

    public String getSummary(){return epic.getSummary();}

    public Date getCreated(){return epic.getCreated();}

    public Date getUpdated(){return epic.getUpdated();}

    public String getDescription(){return epic.getDescription();}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        EpicDTO epicDTO = (EpicDTO) o;
        return Objects.equals(epic, epicDTO.epic);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), epic);
    }
}
