package com.epam.smartman.controller.DTO;

import com.epam.smartman.domain.ProjectUser;

public class ProjectUserDTO {

  ProjectUser projectUser;

  public ProjectUserDTO(ProjectUser projectUser) {
    this.projectUser = projectUser;
  }

  public Long getUserId() {
    return projectUser.getUserId();
  }

  public String getUser() {
    return projectUser.getUser().getUsername() + " " + projectUser.getUser().getSurname();
  }

  public String getProjectRole() {
    return projectUser.getProjectRole().toString();
  }
}
