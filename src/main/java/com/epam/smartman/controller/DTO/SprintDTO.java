package com.epam.smartman.controller.DTO;

import com.epam.smartman.controller.IssueController;
import com.epam.smartman.domain.Sprint;
import com.epam.smartman.exceptions.NoAccessRightsException;
import com.epam.smartman.exceptions.NotAuthorisedUserException;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import java.util.Objects;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class SprintDTO extends ResourceSupport {

  Sprint sprint;

  public SprintDTO(Sprint sprint, Link selfLink)
      throws NotAuthorisedUserException, NoAccessRightsException {
    this.sprint = sprint;
    add(selfLink);
    add(linkTo(methodOn(IssueController.class)
        .getBacklogSprint(sprint.getProject().getId(), sprint.getId())).withRel("sprintBacklog"));
  }

  public Long getSprintId() {
    return sprint.getId();
  }

  public String getName() {
    return sprint.getName();
  }

  public String getStartDate() {
    return sprint.getStartDate().toString();
  }

  public String getDeadline() {
    return sprint.getDeadline().toString();
  }

  public String getDescription() {
    return sprint.getDescription();
  }

  public Long getProjectId() {
    return sprint.getProject().getId();
  }

  public Long getVersionId() {
    return sprint.getVersion() == null ? null : sprint.getVersion().getId();
  }

  public String getCloseDate() {
    return sprint.getCloseDate() == null ? "" : sprint.getCloseDate().toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    if (!super.equals(o)) return false;
    SprintDTO sprintDTO = (SprintDTO) o;
    return Objects.equals(sprint, sprintDTO.sprint);
  }

  @Override
  public int hashCode() {

    return Objects.hash(super.hashCode(), sprint);
  }
}
