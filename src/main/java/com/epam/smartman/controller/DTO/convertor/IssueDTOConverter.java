package com.epam.smartman.controller.DTO.convertor;

import com.epam.smartman.controller.DTO.IssueDTO;
import com.epam.smartman.domain.Issue;

public class IssueDTOConverter {

    public static IssueDTO buildIssueDTO(Issue issue) {
        IssueDTO issueDTO = new IssueDTO();
        issueDTO.setIssueId(issue.getId());
        issueDTO.setSummary(issue.getSummary());
        issueDTO.setDescription(issue.getDescription());
        issueDTO.setStoryPoints(issue.getStoryPoints());
        issueDTO.setReporterId(issue.getReporter().getId());
        issueDTO.setAssigneeId(issue.getAssignee() == null ? null : issue.getAssignee().getId());
        issueDTO.setIssueType(issue.getIssueType().toString());
        issueDTO.setEpicId(issue.getEpic() == null ? null : issue.getEpic().getId());
        issueDTO.setVersionId(issue.getVersion() == null ? null : issue.getVersion().getId());
        issueDTO.setStatus(issue.getStatus().toString());
        issueDTO.setCreated(issue.getCreated().toString());
        issueDTO.setUpdated(issue.getUpdated() == null ? "" : issue.getUpdated().toString());
        issueDTO.setProjectId(issue.getProject().getId());
        issueDTO.setSprintId(issue.getSprint() == null ? null : issue.getSprint().getId());
        issueDTO.setPriority(issue.getPriority().toString());
        issueDTO.setAssignee(issue.getAssignee() == null ? null
                : issue.getAssignee().getUsername() + " " + issue.getAssignee().getSurname());
        issueDTO.setTaskListId(issue.getTaskList().getId());
        return issueDTO;
    }
}

