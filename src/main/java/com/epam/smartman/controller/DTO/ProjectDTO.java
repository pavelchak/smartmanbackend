package com.epam.smartman.controller.DTO;

import com.epam.smartman.controller.IssueController;
import com.epam.smartman.controller.SprintController;
import com.epam.smartman.domain.Project;
import com.epam.smartman.exceptions.NoAccessRightsException;
import com.epam.smartman.exceptions.NotAuthorisedUserException;
import com.epam.smartman.service.SprintService;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import java.util.Objects;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class ProjectDTO extends ResourceSupport {

    private Project project;

    private Long currentSprintId;

    public ProjectDTO(Project project, Link selfLink, SprintService sprintService)
            throws NotAuthorisedUserException, NoAccessRightsException {
        this.project = project;
        add(selfLink);
        add(linkTo(methodOn(IssueController.class)
                .getProjectBacklog(project.getId())).withRel("projectBacklog"));

        currentSprintId = sprintService.getCurrentSprint(project);
        if (currentSprintId != null) {
            add(linkTo(
                    methodOn(SprintController.class).getSprint(project.getId(), currentSprintId))
                    .withRel("currentSprint"));
        }
    }

    public Long getProjectId() {
        return project.getId();
    }

    public String getName() {
        return project.getName();
    }

    public String getShortName() {
        return project.getShortName();
    }

    public String getDescription() {
        return project.getDescription();
    }

    public Long getCurrentSprintId() {
        return currentSprintId;
    }

    public Long getProjectCreatorId() {
        return project.getProjectCreator().getId();
    }

    public boolean isActive() {
        return project.isActive();
    }

    public String getProjectType() {
        return project.getProjectType() == null ? null : project.getProjectType().toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        ProjectDTO dto = (ProjectDTO) o;
        return Objects.equals(project, dto.project);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), project);
    }
}