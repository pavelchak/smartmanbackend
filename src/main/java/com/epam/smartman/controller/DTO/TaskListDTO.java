package com.epam.smartman.controller.DTO;

import com.epam.smartman.domain.TaskList;

import java.util.List;

public class TaskListDTO {

    TaskList taskList;

    List<IssueDTO> issues;

    public TaskListDTO(TaskList taskList) {
        this.taskList = taskList;
    }
    public Long getId() {
        return taskList.getId();
    }

    public String getName() {
        return taskList.getName();
    }

    public Long getProjectId() {
        return taskList.getProject().getId();
    }

    public void setIssues(List<IssueDTO> issues) {
        this.issues = issues;
    }

    public List<IssueDTO> getIssues() {
        return issues;
    }

}