package com.epam.smartman.controller.DTO;

import com.epam.smartman.domain.User;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import java.util.Objects;

public class UserDTO extends ResourceSupport {

  User user;

  public UserDTO(User user, Link selfLink) {
    this.user = user;
    add(selfLink);
  }

  public Long getUserId() {
    return user.getId();
  }

  public String getSurname() {
    return user.getSurname();
  }

  public String getEmail() {
    return user.getEmail();
  }

  public String getAuthority() {
    return user.getAuthority().toString();
  }

  public String getName() {
    return user.getUsername();
  }

  public String getFullName() {
    return user.getUsername() + " " +  user.getSurname();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    UserDTO userDTO = (UserDTO) o;
    return Objects.equals(user, userDTO.user);
  }

  @Override
  public int hashCode() {

    return Objects.hash(super.hashCode(), user);
  }
}