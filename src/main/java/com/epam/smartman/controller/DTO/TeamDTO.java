package com.epam.smartman.controller.DTO;

import com.epam.smartman.controller.UserController;
import com.epam.smartman.domain.Team;
import com.epam.smartman.domain.User;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class TeamDTO extends ResourceSupport {
    private Team team;

    public TeamDTO(Team team, Link selfLink){
        this.team = team;
        add(selfLink);
    }

    public Long getTeamId(){return team.getId();}

    public String getName(){return team.getName();}

    public List<UserDTO> getTeamUsers() {
        Set<User> teamUsers = team.getUsers();
        List<UserDTO> teamUserDTOs = new ArrayList<>();
        for (User teamUser : teamUsers) {
            Link link = linkTo(methodOn(UserController.class).getUser(teamUser.getId())).withSelfRel();
            UserDTO userDTO = new UserDTO(teamUser, link);
            teamUserDTOs.add(userDTO);
        }
        return teamUserDTOs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        TeamDTO teamDTO = (TeamDTO) o;
        return Objects.equals(team, teamDTO.team);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), team);
    }
}
