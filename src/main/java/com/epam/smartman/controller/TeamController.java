package com.epam.smartman.controller;

import com.epam.smartman.controller.DTO.TeamDTO;
import com.epam.smartman.domain.Team;
import com.epam.smartman.service.AccessService;
import com.epam.smartman.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class TeamController {

    @Autowired
    private TeamService teamService;
    @Autowired
    AccessService accessService;

    @GetMapping(value = "/api/team")
    public ResponseEntity<List<TeamDTO>> getAllTeams() {
        List<Team> teams = teamService.getAllTeams();
        Link link = linkTo(methodOn(TeamController.class).getAllTeams()).withSelfRel();

        List<TeamDTO> teamDTOS = new ArrayList<>();
        for (Team team : teams) {
            Link selfLink = new Link(link.getHref() + "/" + team.getId()).withSelfRel();
            TeamDTO dto = new TeamDTO(team, selfLink);
            teamDTOS.add(dto);
        }

        return new ResponseEntity<>(teamDTOS, HttpStatus.OK);
    }

    @GetMapping(value = "/api/team/{teamId}")
    public ResponseEntity<TeamDTO> getTeam(@PathVariable("teamId") Long teamId) {
        Team team = teamService.readTeam(teamId);
        Link link = linkTo(methodOn(TeamController.class).getTeam(teamId)).withSelfRel();

        TeamDTO dto = new TeamDTO(team, link);

        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping(value = "/api/team")
    public ResponseEntity<TeamDTO> addTeam(@RequestBody Team newTeam) {
        Team team = teamService.createTeam(newTeam);
        Link link = linkTo(methodOn(TeamController.class).getTeam(team.getId())).withSelfRel();

        TeamDTO dto = new TeamDTO(team, link);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/api/team/{teamId}")
    public ResponseEntity deleteTeam(@PathVariable("teamId") Long teamId) {
        teamService.deleteTeam(teamId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping(value = "/api/team/{teamId}")
    public ResponseEntity<TeamDTO> updateTeam(@RequestBody Team updatedTeam,
                                              @PathVariable(value = "teamId") Long teamId) {
        teamService.updateTeam(teamId, updatedTeam);
        Team project = teamService.readTeam(teamId);

        Link link = linkTo(methodOn(TeamController.class).getTeam(teamId)).withSelfRel();

        TeamDTO dto = new TeamDTO(project, link);

        return new ResponseEntity<>(dto, HttpStatus.OK);
    }


}
