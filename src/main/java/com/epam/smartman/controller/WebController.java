package com.epam.smartman.controller;

import com.epam.smartman.exceptions.NotAuthorisedUserException;
import com.epam.smartman.service.AccessService;
import com.epam.smartman.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class WebController {
    private static final String anonymoususer = "ANONYMOUSUSER";

    @Autowired
    AccessService accessService;
    @Autowired
    TeamService teamService;


    @GetMapping("/")
    public String mainPage() {
        return "index.html";
    }

    @GetMapping("/login-page")
    public String loginPage() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        boolean authenticated = authentication.isAuthenticated();

            if (authenticated && !authentication.getName().equalsIgnoreCase(anonymoususer)) {
                return "index.html";
            } else {
   
                return "login.html";
            }
        }

    @GetMapping("/loginerror")
    public String loginError() {
        return "login-error.html";
    }


    @GetMapping("/toAdminPage")
    public String adminPage() throws NotAuthorisedUserException {

        boolean ifUserIsAdm = accessService.checkAuthorityForAdmin();
        if (ifUserIsAdm) {
            return "admin-page.html";
        } else {
            return "index.html";
        }
    }
}
