package com.epam.smartman.controller;

import com.epam.smartman.controller.DTO.MessageDTO;
import com.epam.smartman.exceptions.NoAccessRightsException;
import com.epam.smartman.exceptions.NotAllowedOperationException;
import com.epam.smartman.exceptions.NotAuthorisedUserException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.NoSuchElementException;

@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

  @ExceptionHandler(NoSuchElementException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ResponseBody
  MessageDTO handleNoSuchElementException(NoSuchElementException noSuchElementException) {
    return new MessageDTO(noSuchElementException.getMessage());
  }

  @ExceptionHandler(IllegalArgumentException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  MessageDTO handleIllegalArgumentException(IllegalArgumentException illegalArgumentException) {
    return new MessageDTO(illegalArgumentException.getMessage());
  }

  @ExceptionHandler(PermissionDeniedDataAccessException.class)
  @ResponseStatus(HttpStatus.FORBIDDEN)
  MessageDTO handleForbiden403Error(PermissionDeniedDataAccessException pddax) {
    return new MessageDTO(pddax.getMessage());
  }

  @ExceptionHandler(Exception.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  MessageDTO handleInternal500Error(Exception ex) {
    return new MessageDTO(ex.getMessage());
  }

  @ExceptionHandler(NotAllowedOperationException.class)
  @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
  @ResponseBody
  MessageDTO handleNotAllowedOperationException(NotAllowedOperationException exception) {
    return new MessageDTO(exception.getMessage());
  }

  @ExceptionHandler(NotAuthorisedUserException.class)
  @ResponseStatus(HttpStatus.UNAUTHORIZED)
  @ResponseBody
  MessageDTO handleNotAuthorisedUserException() {
    return new MessageDTO("Operation is unavailable for unauthorized users!");
  }

  @ExceptionHandler(NoAccessRightsException.class)
  @ResponseStatus(HttpStatus.UNAUTHORIZED)
  @ResponseBody
  MessageDTO handleNoAccessRightsException(NoAccessRightsException exception) {
    return new MessageDTO(exception.getMessage());
  }
}
