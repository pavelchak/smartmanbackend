package com.epam.smartman.controller;

import com.epam.smartman.controller.DTO.*;
import com.epam.smartman.controller.DTO.convertor.IssueDTOConverter;
import com.epam.smartman.domain.*;
import com.epam.smartman.domain.enums.ProjectRole;
import com.epam.smartman.domain.enums.ProjectType;
import com.epam.smartman.exceptions.NoAccessRightsException;
import com.epam.smartman.exceptions.NotAuthorisedUserException;
import com.epam.smartman.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class ProjectController {

    @Autowired
    ProjectService projectService;
    @Autowired
    SprintService sprintService;
    @Autowired
    AccessService accessService;
    @Autowired
    TaskListService taskListService;
    @Autowired
    IssueService issueService;

    @GetMapping(value = "/api/project")
    public ResponseEntity<List<ProjectDTO>> getAllProjects()
            throws NotAuthorisedUserException, NoAccessRightsException {
        User user = accessService.getAuthorizedUser();
        List<Project> projects = projectService.getUserProjects(user);
        Link link = linkTo(methodOn(ProjectController.class).getAllProjects()).withSelfRel();
        List<ProjectDTO> projectDTOList = new ArrayList<>();
        for (Project project : projects) {
            Link selfLink = new Link(link.getHref() + "/" + project.getId()).withSelfRel();
            ProjectDTO dto = new ProjectDTO(project, selfLink, sprintService);
            projectDTOList.add(dto);
        }
        return new ResponseEntity<>(projectDTOList, HttpStatus.OK);
    }

    @PostMapping(value = "/api/project/{projectId}")
    public ResponseEntity<ProjectDTO> updateProject(@RequestBody Project updatedProject,
                                                    @PathVariable(value = "projectId") Long projectId)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.WRITER);
        projectService.updateProject(project, updatedProject);
        Link link = linkTo(methodOn(ProjectController.class).getProject(projectId)).withSelfRel();
        ProjectDTO dto = new ProjectDTO(project, link, sprintService);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "api/project/{projectId}")
    public ResponseEntity<ProjectDTO> getProject(@PathVariable("projectId") Long projectId)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.READER);
        Link link = linkTo(methodOn(ProjectController.class).getProject(projectId)).withSelfRel();
        ProjectDTO dto = new ProjectDTO(project, link, sprintService);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

/*    @PostMapping(value = "api/project")
    public ResponseEntity<ProjectDTO> addProject(@RequestBody Project newProject)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.createProject(newProject);
        Link link = linkTo(methodOn(ProjectController.class).getProject(project.getId()))
                .withSelfRel();
        ProjectDTO dto = new ProjectDTO(project, link, sprintService);
        return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }*/

    @PostMapping(value = "api/project")
    public ResponseEntity<ProjectDTO> addProject(@RequestBody Project newProject)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.createProject(newProject);
        if (project.getProjectType() == ProjectType.SCRUM) {
            taskListService.addScrumTaskLists(project);
        }
        Link link = linkTo(methodOn(ProjectController.class).getProject(project.getId()))
                .withSelfRel();
        ProjectDTO dto = new ProjectDTO(project, link, sprintService);
        return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }

    @GetMapping(value = "/api/project/{projectId}/card-lists")
    public ResponseEntity<List<TaskListDTO>> getTaskLists(@PathVariable Long projectId)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.READER);

        List<TaskList> taskLists = taskListService.getTaskLists(project);
        List<TaskListDTO> taskListDTOS = new ArrayList<>();

        for (TaskList taskList : taskLists) {
            TaskListDTO taskListDTO = new TaskListDTO(taskList);
            List<Issue> issues = issueService.getTaskList(project, taskList.getId());
            List<IssueDTO> issueDTOs = new ArrayList<>();
            for (Issue issue : issues) {
                IssueDTO issueDTO = IssueDTOConverter.buildIssueDTO(issue);
                issueDTOs.add(issueDTO);
            }
            taskListDTO.setIssues(issueDTOs);
            taskListDTOS.add(taskListDTO);
        }

        return new ResponseEntity<>(taskListDTOS, HttpStatus.OK);
    }

    @DeleteMapping(value = "/api/project/{projectId}")
    public ResponseEntity deleteProject(@PathVariable("projectId") Long projectId)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.CREATOR);
        projectService.deleteProject(project);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/api/project/{projectId}/team/{teamId}", method = RequestMethod.PATCH)
    public ResponseEntity<ProjectDTO> addTeam(@PathVariable(value = "teamId") Long teamId,
                                              @PathVariable(value = "projectId") Long projectId)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        projectService.addTeam(projectId, teamId);
        Link link = linkTo(methodOn(ProjectController.class).getProject(projectId)).withSelfRel();
        ProjectDTO dto = new ProjectDTO(project, link, sprintService);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/api/project/{projectId}/team/{teamId}")
    public ResponseEntity<ProjectDTO> removeTeam(@PathVariable(value = "teamId") Long teamId,
                                                 @PathVariable(value = "projectId") Long projectId)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        projectService.removeTeam(projectId, teamId);
        Link link = linkTo(methodOn(ProjectController.class).getProject(projectId)).withSelfRel();
        ProjectDTO dto = new ProjectDTO(project, link, sprintService);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/api/project/{projectId}/teams")
    public ResponseEntity<List<TeamDTO>> getProjectTeams(
            @PathVariable(value = "projectId") Long projectId)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.READER);
        Set<Team> projectTeams = project.getTeams();
        List<TeamDTO> projectTeamDTOs = new ArrayList<>();
        Link link = linkTo(methodOn(ProjectController.class).getProject(projectId)).withSelfRel();
        for (Team team : projectTeams) {
            TeamDTO teamDTO = new TeamDTO(team, link);
            projectTeamDTOs.add(teamDTO);
        }
        return new ResponseEntity<>(projectTeamDTOs, HttpStatus.OK);
    }

    @PostMapping(value = "/api/project/{projectId}/user/{userId}/role/{projectRole}")
    public ResponseEntity addUserToProject(
            @PathVariable(value = "projectId") Long projectId,
            @PathVariable(value = "userId") Long userId,
            @PathVariable(value = "projectRole") String projectRole)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.CREATOR);
        projectService.addUserToProject(project, userId, ProjectRole.valueOf(projectRole));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(value = "/api/project/{projectId}/user/{userId}")
    public ResponseEntity removeUserFromProject(@PathVariable(value = "projectId") Long projectId,
                                                @PathVariable(value = "userId") Long userId)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.CREATOR);
        projectService.removeUserFromProject(project, userId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/api/project/{projectId}/user/{userId}/role/{projectRole}")
    public ResponseEntity changeProjectUserRole(
            @PathVariable(value = "projectId") Long projectId,
            @PathVariable(value = "userId") Long userId,
            @PathVariable(value = "projectRole") String projectRole)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.WRITER);
        projectService.changeProjectUserRole(project, userId, ProjectRole.valueOf(projectRole));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/api/project/{projectId}/projectUsers")
    public ResponseEntity<List<ProjectUserDTO>> getProjectUsers(
            @PathVariable(value = "projectId") Long projectId)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.READER);
        List<ProjectUser> users = projectService.getProjectUsers(project);
        List<ProjectUserDTO> projectUserDTOList = new ArrayList<>();
        for (ProjectUser user : users) {
            ProjectUserDTO dto = new ProjectUserDTO(user);
            projectUserDTOList.add(dto);
        }
        return new ResponseEntity<>(projectUserDTOList, HttpStatus.OK);
    }

    @PostMapping(value = "/api/project/{projectId}/card-list/{nameTaskList}")
    public ResponseEntity<TaskListDTO> addTaskListToProject(
            @PathVariable(value = "projectId") Long projectId,
            @PathVariable(value = "nameTaskList") String nameTaskList)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.CREATOR);
        TaskList taskList = taskListService.addTaskList(project, nameTaskList);
        TaskListDTO dto = new TaskListDTO(taskList);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/project/{projectId}/isScrum")
    public boolean isCustomizable(@PathVariable(value = "projectId") Long projectId)
            throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.READER);
        return project.getProjectType() == ProjectType.KANBAN;
    }

    @PutMapping(value = "/api/project/{projectId}/card-list/{cardListId}/{cardListName}")
    public ResponseEntity changeCardListName(
            @PathVariable(value = "projectId") Long projectId,
            @PathVariable(value = "cardListId") Long cardListId,
            @PathVariable(value = "cardListName") String cardListName) throws NotAuthorisedUserException, NoAccessRightsException {
        Project project = projectService.getProject(projectId);
        accessService.checkAccessRights(project, ProjectRole.WRITER);
        taskListService.changeTaskListName(cardListId, cardListName);
        return new ResponseEntity(HttpStatus.OK);
    }

}