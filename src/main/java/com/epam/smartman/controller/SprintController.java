package com.epam.smartman.controller;

import com.epam.smartman.controller.DTO.SprintDTO;
import com.epam.smartman.domain.Project;
import com.epam.smartman.domain.Sprint;
import com.epam.smartman.domain.enums.ProjectRole;
import com.epam.smartman.exceptions.NoAccessRightsException;
import com.epam.smartman.exceptions.NotAllowedOperationException;
import com.epam.smartman.exceptions.NotAuthorisedUserException;
import com.epam.smartman.service.AccessService;
import com.epam.smartman.service.ProjectService;
import com.epam.smartman.service.SprintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class SprintController {

  @Autowired
  SprintService sprintService;
  @Autowired
  ProjectService projectService;
  @Autowired
  AccessService accessService;

  @GetMapping(value = "/api/project/{projectId}/sprint")
  public ResponseEntity<List<SprintDTO>> getProjectSprints(@PathVariable Long projectId)
      throws NotAuthorisedUserException, NoAccessRightsException {
    Project project = projectService.getProject(projectId);
    accessService.checkAccessRights(project, ProjectRole.READER);
    List<Sprint> sprintList = sprintService.getProjectSprints(project);
    Link link = linkTo(methodOn(SprintController.class).getProjectSprints(projectId)).withSelfRel();
    List<SprintDTO> sprintDTOList = new ArrayList<>();
    for (Sprint entity : sprintList) {
      Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
      SprintDTO dto = new SprintDTO(entity, selfLink);
      sprintDTOList.add(dto);
    }
    return new ResponseEntity<>(sprintDTOList, HttpStatus.OK);
  }

  // TODO
  // rewrite without using projectId
  @GetMapping(value = "/api/project/{projectId}/sprint/{sprintId}")
  public ResponseEntity<SprintDTO> getSprint(@PathVariable Long projectId,
      @PathVariable Long sprintId) throws NotAuthorisedUserException, NoAccessRightsException {
    Project project = projectService.getProject(projectId);
    accessService.checkAccessRights(project, ProjectRole.READER);
    Sprint sprint = sprintService.getSprint(project, sprintId);
    Link link = linkTo(methodOn(SprintController.class).getSprint(projectId, sprintId))
        .withSelfRel();
    SprintDTO sprintDTO = new SprintDTO(sprint, link);
    return new ResponseEntity<>(sprintDTO, HttpStatus.OK);
  }

  @PostMapping(value = "/api/project/{projectId}/sprint/version/{versionId}")
  public ResponseEntity<SprintDTO> addSprint(@PathVariable Long projectId,
      @PathVariable Long versionId, @RequestBody Sprint newSprint)
      throws NotAllowedOperationException, NotAuthorisedUserException, NoAccessRightsException {
    Project project = projectService.getProject(projectId);
    accessService.checkAccessRights(project, ProjectRole.WRITER);
    Sprint sprint = sprintService.createSprint(project, versionId, newSprint);
    Link link = linkTo(methodOn(SprintController.class).getSprint(projectId, sprint.getId()))
        .withSelfRel();
    SprintDTO sprintDTO = new SprintDTO(sprint, link);
    return new ResponseEntity<>(sprintDTO, HttpStatus.CREATED);
  }

  @PutMapping(value = "/api/project/{projectId}/sprint/{sprintId}/version/{versionId}")
  public ResponseEntity<SprintDTO> updateSprint(@PathVariable Long projectId,
      @PathVariable Long sprintId, @PathVariable Long versionId, @RequestBody Sprint updSprint)
      throws NotAuthorisedUserException, NoAccessRightsException {
    Project project = projectService.getProject(projectId);
    accessService.checkAccessRights(project, ProjectRole.WRITER);
    Sprint sprint = sprintService.updateSprint(project, updSprint, sprintId, versionId);
    Link link = linkTo(methodOn(SprintController.class).getSprint(projectId, sprint.getId()))
        .withSelfRel();
    SprintDTO sprintDTO = new SprintDTO(sprint, link);
    return new ResponseEntity<>(sprintDTO, HttpStatus.OK);
  }

  @PutMapping(value = "/api/project/{projectId}/closesprint/{sprintId}")
  public ResponseEntity<SprintDTO> closeSprint(@PathVariable Long projectId,
      @PathVariable Long sprintId)
      throws NotAllowedOperationException, NotAuthorisedUserException, NoAccessRightsException {
    Project project = projectService.getProject(projectId);
    accessService.checkAccessRights(project, ProjectRole.WRITER);
    Sprint sprint = sprintService.closeSprint(project, sprintId);
    Link link = linkTo(methodOn(SprintController.class).getSprint(projectId, sprint.getId()))
        .withSelfRel();
    SprintDTO sprintDTO = new SprintDTO(sprint, link);
    return new ResponseEntity<>(sprintDTO, HttpStatus.OK);
  }

  @DeleteMapping(value = "/api/project/{projectId}/sprint/{sprintId}")
  public ResponseEntity deleteSprint(@PathVariable Long projectId, @PathVariable Long sprintId)
      throws NotAuthorisedUserException, NoAccessRightsException {
    Project project = projectService.getProject(projectId);
    accessService.checkAccessRights(project, ProjectRole.WRITER);
    sprintService.deleteSprint(project, sprintId);
    return new ResponseEntity(HttpStatus.OK);
  }
}
