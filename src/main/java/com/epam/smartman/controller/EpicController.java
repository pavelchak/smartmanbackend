package com.epam.smartman.controller;

import com.epam.smartman.controller.DTO.EpicDTO;
import com.epam.smartman.domain.Epic;
import com.epam.smartman.service.EpicService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.hateoas.Link;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class EpicController {

  @Autowired
  private EpicService epicService;


  @GetMapping(value = "/api/epic")
  public ResponseEntity<List<EpicDTO>> getAllEpics() {
    List<Epic> epics = epicService.getAllEpics();
    Link link = linkTo(methodOn(TeamController.class).getAllTeams()).withSelfRel();

    List<EpicDTO> epicDTOS = new ArrayList<>();
    for (Epic epic : epics) {
      Link selfLink = new Link(link.getHref() + "/" + epic.getId()).withSelfRel();
      EpicDTO dto = new EpicDTO(epic, selfLink);
      epicDTOS.add(dto);
    }

    return new ResponseEntity<>(epicDTOS, HttpStatus.OK);
  }

  @GetMapping(value = "/api/epic/{epicId}")
  public ResponseEntity<EpicDTO> getEpic(@PathVariable("epicId") Long epicId) {
    Epic epic = epicService.readEpic(epicId);
    Link link = linkTo(methodOn(EpicController.class).getEpic(epicId)).withSelfRel();

    EpicDTO dto = new EpicDTO(epic, link);

    return new ResponseEntity<>(dto, HttpStatus.OK);
  }

  @PostMapping(value = "/api/epic")
  public ResponseEntity<EpicDTO> addEpic(@RequestBody Epic newEpic) {
    epicService.createEpic(newEpic);
    Link link = linkTo(methodOn(EpicController.class).getEpic(newEpic.getId())).withSelfRel();

    EpicDTO dto = new EpicDTO(newEpic, link);
    return new ResponseEntity<>(dto, HttpStatus.CREATED);
  }

  @DeleteMapping(value = "/api/epic/{epicId}")
  public ResponseEntity deleteEpic(@PathVariable("epicId") Long epicId) {
    epicService.deleteEpic(epicId);
    return new ResponseEntity(HttpStatus.OK);
  }

  @PutMapping(value = "/api/epic/{epicId}")
  public ResponseEntity<EpicDTO> updateEpic(@RequestBody Epic updatedEpic,
      @PathVariable(value = "epicId") Long epicId) {
    epicService.updateEpic(epicId, updatedEpic);
    Epic epic = epicService.readEpic(epicId);

    Link link = linkTo(methodOn(EpicController.class).getEpic(epicId)).withSelfRel();

    EpicDTO dto = new EpicDTO(epic, link);

    return new ResponseEntity<>(dto, HttpStatus.OK);
  }
}
