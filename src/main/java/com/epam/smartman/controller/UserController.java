package com.epam.smartman.controller;

import com.epam.smartman.controller.DTO.UserDTO;
import com.epam.smartman.domain.User;
import com.epam.smartman.exceptions.NotAuthorisedUserException;
import com.epam.smartman.service.AccessService;
import com.epam.smartman.service.Impl.GeneratePasswordServiceImpl;
import com.epam.smartman.service.MailSendService;
import com.epam.smartman.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.security.Principal;


import java.util.ArrayList;
import java.util.List;


import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class UserController {

    @Autowired
    UserService userService;
    @Autowired
    AccessService accessService;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    private MailSendService mailSendService;
    @Autowired
    private GeneratePasswordServiceImpl passwordService;

    @GetMapping(value = "/api/user")
    public ResponseEntity<List<UserDTO>> getAllUsers() {
        List<User> userList = userService.getAllUsers();
        Link link = linkTo(methodOn(UserController.class).getAllUsers()).withSelfRel();
        if (userList.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        List<UserDTO> userDTOList = new ArrayList<>();
        for (User entity : userList) {
            Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
            UserDTO dto = new UserDTO(entity, selfLink);
            userDTOList.add(dto);
        }
        return new ResponseEntity<>(userDTOList, HttpStatus.OK);
    }

    @PostMapping(value = "/api/user")
    public ResponseEntity<UserDTO> addUser(@RequestBody @Valid User newUser) {
        if (userService.isUserExist(newUser)) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
        newUser.setPassword(passwordService.generatePass());
        String encode = passwordEncoder.encode(newUser.getPassword());
        mailSendService.sendEmail(newUser);
        newUser.setPassword(encode);
        userService.save(newUser);
        Link link = linkTo(methodOn(UserController.class).getUser(newUser.getId())).withSelfRel();
        UserDTO userDTO = new UserDTO(newUser, link);
        return new ResponseEntity<>(userDTO, HttpStatus.CREATED);
    }

    @PutMapping(value = "/api/user/{userId}")
    public ResponseEntity<UserDTO> updateUser(@RequestBody User updatedUser,
                                              @PathVariable Long userId) {
        userService.updateUser(updatedUser, userId);
        User user = userService.getUser(userId);
        Link link = linkTo(methodOn(UserController.class).getUser(userId)).withSelfRel();
        UserDTO userDTO = new UserDTO(user, link);
        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

    @PutMapping(value = "/api/user/update/password")
    public ResponseEntity<UserDTO> updateUserPassword(
            @RequestBody String password, Principal principal) {
        User user = userService.findByEmail(principal.getName());
        user.setPassword(passwordEncoder.encode(password));
        userService.save(user);
        Link link = linkTo(methodOn(UserController.class).getUser(user.getId())).withSelfRel();
        UserDTO userDTO = new UserDTO(user, link);
        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/api/user/{userId}")
    public ResponseEntity deleteUser(@PathVariable Long userId) {
        User user = userService.findById(userId);
        if (user == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        userService.deleteUser(userId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping(value = "/api/user/{userId}")
    public ResponseEntity<UserDTO> getUser(@PathVariable Long userId) {
        User user = userService.getUser(userId);
        Link link = linkTo(methodOn(UserController.class).getUser(userId)).withSelfRel();
        if (user == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        UserDTO userDTO = new UserDTO(user, link);
        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/api/user/{userId}/team/{teamId}")
    public ResponseEntity addTeamToUser(@PathVariable(value = "userId") Long userId,
                                        @PathVariable(value = "teamId") Long teamId) throws NotAuthorisedUserException {
        accessService.getAuthorizedUser();
        userService.addTeam(userId, teamId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(value = "/api/user/{userId}/team/{teamId}")
    public ResponseEntity removeTeamFromUser(@PathVariable(value = "userId") Long userId,
                                             @PathVariable(value = "teamId")
                                                     Long teamId) throws NotAuthorisedUserException {
        accessService.getAuthorizedUser();
        userService.removeTeam(userId, teamId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/api/user_like/{pattern}")
    public ResponseEntity<List<UserDTO>> getUsersLike(@PathVariable(value = "pattern") String pattern)
            throws NotAuthorisedUserException {
        accessService.getAuthorizedUser();
        List<User> users = userService.getUsersLike(pattern);
        Link link = linkTo(methodOn(UserController.class).getAllUsers()).withSelfRel();
        List<UserDTO> userDTOList = new ArrayList<>();
        for (User entity : users) {
            Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
            UserDTO dto = new UserDTO(entity, selfLink);
            userDTOList.add(dto);
        }
        return new ResponseEntity<>(userDTOList, HttpStatus.OK);
    }

    @GetMapping(value = "/api/authorized-user")
    public ResponseEntity<UserDTO> getAuthorizedUser(Principal principal) {
        User user = userService.findByEmail(principal.getName());
        Link link = linkTo(methodOn(UserController.class).getUser(user.getId())).withSelfRel();
        UserDTO userDTO = new UserDTO(user, link);
        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

}