package com.epam.smartman.exceptions;

public class NoAccessRightsException extends Exception {

  public NoAccessRightsException(String message) {
    super(message);
  }
}
