package com.epam.smartman.exceptions;

public class NotAllowedOperationException extends Exception {

  public NotAllowedOperationException(String message) {
    super(message);
  }
}
