CREATE TABLE account (
  id       BIGINT       NOT NULL,
  email    VARCHAR(70)  NOT NULL,
  password VARCHAR(100) NOT NULL,
  updated  DATETIME,
  role     VARCHAR(15)  NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = INNODB;

CREATE TABLE authority (
  role VARCHAR(15) NOT NULL,
  PRIMARY KEY (role)
)
  ENGINE = INNODB;

CREATE TABLE comment (
  id       BIGINT AUTO_INCREMENT,
  contents MEDIUMTEXT           NOT NULL,
  user_id  BIGINT               NOT NULL,
  type     VARCHAR(15)          NOT NULL,
  edited   BIT(1) DEFAULT FALSE NOT NULL,
  issue_id BIGINT,
  epic_id  BIGINT,
  PRIMARY KEY (id)
)
  ENGINE = INNODB;

CREATE TABLE comment_type (
  type VARCHAR(15) NOT NULL,
  PRIMARY KEY (type)
)
  ENGINE = INNODB;

CREATE TABLE epic (
  id          BIGINT AUTO_INCREMENT,
  name        VARCHAR(50)  NOT NULL,
  summary     VARCHAR(255) NOT NULL,
  status      VARCHAR(15)  NOT NULL,
  created     DATETIME     NOT NULL,
  updated     DATETIME,
  reporter_id BIGINT       NOT NULL,
  assignee_id BIGINT,
  priority_id BIGINT       NOT NULL,
  descriprion MEDIUMTEXT,
  project_id  BIGINT       NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = INNODB;

CREATE TABLE team (
  id   BIGINT AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = INNODB;

CREATE TABLE team_project (
  team_id    BIGINT NOT NULL,
  project_id BIGINT NOT NULL,
  PRIMARY KEY (team_id, project_id)
)
  ENGINE = INNODB;

CREATE TABLE team_user (
  team_id BIGINT NOT NULL,
  user_id BIGINT NOT NULL,
  PRIMARY KEY (team_id, user_id)
)
  ENGINE = INNODB;

CREATE TABLE task_list (
  id         BIGINT NOT NULL,
  name       VARCHAR(100) NOT NULL,
  project_id BIGINT       NOT NULL
)
  ENGINE = INNODB;

CREATE TABLE issue (
  id           BIGINT AUTO_INCREMENT,
  summary      VARCHAR(255) NOT NULL,
  status       VARCHAR(15)  NOT NULL,
  reporter_id  BIGINT, -- changed
  assignee_id  BIGINT,
  created      DATETIME, -- changed
  updated      DATETIME,
  priority_id  BIGINT, -- changed
  descriprion  MEDIUMTEXT,
  story_points INT,
  sprint_id    BIGINT,
  project_id   BIGINT       NOT NULL,
  type         VARCHAR(10), -- changed
  epic_id      BIGINT,
  version_id   BIGINT,
  task_list_id BIGINT NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = INNODB;

CREATE TABLE issue_type (
  type VARCHAR(10) NOT NULL,
  PRIMARY KEY (type)
)
  ENGINE = INNODB;

CREATE TABLE priority (
  id   BIGINT AUTO_INCREMENT,
  name VARCHAR(10) NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = INNODB;

CREATE TABLE project (
  id                 BIGINT AUTO_INCREMENT,
  name               VARCHAR(50) NOT NULL,
  short_name         VARCHAR(10) NOT NULL,
  avatar             MEDIUMBLOB,
  description        MEDIUMTEXT,
  project_creator_id BIGINT      NOT NULL,
  active             BOOLEAN,
  PRIMARY KEY (id)
)
  ENGINE = INNODB;

CREATE TABLE project_role (
  role VARCHAR(20) NOT NULL,
  PRIMARY KEY (role)
)
  ENGINE = INNODB;

CREATE TABLE project_user (
  project_id BIGINT      NOT NULL,
  user_id    BIGINT      NOT NULL,
  role       VARCHAR(20) NOT NULL,
  PRIMARY KEY (project_id, user_id)
)
  ENGINE = INNODB;

CREATE TABLE sprint (
  id          BIGINT AUTO_INCREMENT,
  name        VARCHAR(50) NOT NULL,
  start_date  DATE        NOT NULL,
  deadline    DATE        NOT NULL,
  description VARCHAR(4000),
  project_id  BIGINT      NOT NULL,
  version_id  BIGINT,
  PRIMARY KEY (id)
)
  ENGINE = INNODB;

CREATE TABLE status (
  status VARCHAR(15) NOT NULL,
  PRIMARY KEY (status)
)
  ENGINE = INNODB;

CREATE TABLE user (
  id      BIGINT AUTO_INCREMENT,
  surname VARCHAR(50) NOT NULL,
  name    VARCHAR(50) NOT NULL,
  photo   MEDIUMBLOB,
  PRIMARY KEY (id)
)
  ENGINE = INNODB;

CREATE TABLE `version` (
  id           BIGINT AUTO_INCREMENT,
  name         VARCHAR(50)          NOT NULL,
  description  VARCHAR(255),
  start_date   DATE                 NOT NULL,
  release_date DATE                 NOT NULL,
  project_id   BIGINT               NOT NULL,
  released     BIT(1) DEFAULT FALSE NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = INNODB;

ALTER TABLE account
  ADD CONSTRAINT Refuser13
FOREIGN KEY (id)
REFERENCES user (id);

ALTER TABLE account
  ADD CONSTRAINT Refauthority23
FOREIGN KEY (role)
REFERENCES authority (role)
  ON UPDATE CASCADE;

ALTER TABLE comment
  ADD CONSTRAINT Refuser103
FOREIGN KEY (user_id)
REFERENCES user (id);

ALTER TABLE comment
  ADD CONSTRAINT Refcomment_type223
FOREIGN KEY (type)
REFERENCES comment_type (type)
  ON UPDATE CASCADE;

ALTER TABLE comment
  ADD CONSTRAINT Refissue373
FOREIGN KEY (issue_id)
REFERENCES issue (id);

ALTER TABLE comment
  ADD CONSTRAINT Refepic383
FOREIGN KEY (epic_id)
REFERENCES epic (id);

ALTER TABLE epic
  ADD CONSTRAINT Refpriority423
FOREIGN KEY (priority_id)
REFERENCES priority (id);

ALTER TABLE epic
  ADD CONSTRAINT Refuser443
FOREIGN KEY (reporter_id)
REFERENCES user (id);

ALTER TABLE epic
  ADD CONSTRAINT Refuser463
FOREIGN KEY (assignee_id)
REFERENCES user (id);

ALTER TABLE epic
  ADD CONSTRAINT Refproject603
FOREIGN KEY (project_id)
REFERENCES project (id);

ALTER TABLE epic
  ADD CONSTRAINT Refstatus623
FOREIGN KEY (status)
REFERENCES status (status);

ALTER TABLE team_project
  ADD CONSTRAINT Refgroup583
FOREIGN KEY (team_id)
REFERENCES team (id);

ALTER TABLE team_project
  ADD CONSTRAINT Refproject593
FOREIGN KEY (project_id)
REFERENCES project (id);

ALTER TABLE team_user
  ADD CONSTRAINT Refgroup563
FOREIGN KEY (team_id)
REFERENCES team (id);

ALTER TABLE team_user
  ADD CONSTRAINT Refuser573
FOREIGN KEY (user_id)
REFERENCES user (id);

ALTER TABLE issue
  ADD CONSTRAINT Refuser63
FOREIGN KEY (assignee_id)
REFERENCES user (id);

ALTER TABLE issue
  ADD CONSTRAINT Refuser73
FOREIGN KEY (reporter_id)
REFERENCES user (id);

ALTER TABLE issue
  ADD CONSTRAINT Refsprint123
FOREIGN KEY (sprint_id)
REFERENCES sprint (id);

ALTER TABLE issue
  ADD CONSTRAINT Refpriority143
FOREIGN KEY (priority_id)
REFERENCES priority (id);

ALTER TABLE issue
  ADD CONSTRAINT Refproject173
FOREIGN KEY (project_id)
REFERENCES project (id);

ALTER TABLE issue
  ADD CONSTRAINT Refissue_type283
FOREIGN KEY (type)
REFERENCES issue_type (type)
  ON UPDATE CASCADE;

ALTER TABLE issue
  ADD CONSTRAINT Refepic303
FOREIGN KEY (epic_id)
REFERENCES epic (id);

ALTER TABLE issue
  ADD CONSTRAINT Refversion323
FOREIGN KEY (version_id)
REFERENCES `version` (id);

ALTER TABLE issue
  ADD CONSTRAINT Refstatus613
FOREIGN KEY (status)
REFERENCES status (status);

ALTER TABLE project
  ADD CONSTRAINT Refuser153
FOREIGN KEY (project_creator_id)
REFERENCES user (id);

ALTER TABLE project_user
  ADD CONSTRAINT Refuser533
FOREIGN KEY (user_id)
REFERENCES user (id);

ALTER TABLE project_user
  ADD CONSTRAINT Refproject543
FOREIGN KEY (project_id)
REFERENCES project (id);

ALTER TABLE project_user
  ADD CONSTRAINT Refproject_role553
FOREIGN KEY (role)
REFERENCES project_role (role)
  ON UPDATE CASCADE;

ALTER TABLE sprint
  ADD CONSTRAINT Refproject163
FOREIGN KEY (project_id)
REFERENCES project (id);

ALTER TABLE sprint
  ADD CONSTRAINT Refversion433
FOREIGN KEY (version_id)
REFERENCES `version` (id);

ALTER TABLE version
  ADD CONSTRAINT Refproject313
FOREIGN KEY (project_id)
REFERENCES project (id);
