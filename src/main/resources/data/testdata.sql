USE smartman;

DELETE FROM project_user;
DELETE FROM team_user;
DELETE FROM team_project;
DELETE FROM team;
DELETE FROM user;
DELETE FROM comment;
DELETE FROM issue;
DELETE FROM epic;
DELETE FROM sprint;
DELETE FROM version;
DELETE FROM project;
DELETE FROM task_list;

INSERT INTO `user` (id, surname, name, email, password, authority)
VALUES
  (1, 'Smart', 'Man', 'smartman.epam@gmail.com', '$2a$10$hfL1YcYGcXk9rKU5Szsg9eH5/mxWJTom3dRpd.DbuSLtGDD3TZxAm', 'ROLE_ADMIN'),
  (2, 'Boiko', 'Vitalii', 'vitalii.boiko26@gmail.com', '$2a$10$/HOt8euzARRNMAIEMEY3ouY.pPW8xJ8N/MhzjLZ0lGtm7i0MZrbLW', 'ROLE_USER'
   ),
  (3, 'Kogut', 'Nazarii', 'kogut825@gmail.com', '$2a$10$/HOt8euzARRNMAIEMEY3ouY.pPW8xJ8N/MhzjLZ0lGtm7i0MZrbLW', 'ROLE_USER'),
  (4, 'Hlukhotskyy', 'Valentyn', 'nojka@ukr.net', '$2a$10$/HOt8euzARRNMAIEMEY3ouY.pPW8xJ8N/MhzjLZ0lGtm7i0MZrbLW', 'ROLE_USER'
   ),
  (5, 'Pavelchak', 'Andrii', 'apavelchak@gmail.com', '$2a$10$/HOt8euzARRNMAIEMEY3ouY.pPW8xJ8N/MhzjLZ0lGtm7i0MZrbLW', 'ROLE_USER'
   ),
  (6, 'Bernakevych', 'Iryna', 'ibernakevych@gmail.com', '$2a$10$/HOt8euzARRNMAIEMEY3ouY.pPW8xJ8N/MhzjLZ0lGtm7i0MZrbLW', 'ROLE_USER'
   ),
  (7, 'Brynyarskyy', 'Oleg', 'brynyarsky@gmail.com', '$2a$10$/HOt8euzARRNMAIEMEY3ouY.pPW8xJ8N/MhzjLZ0lGtm7i0MZrbLW', 'ROLE_USER'
   ),
  (8, 'Verhun', 'Volodymyr', 'verhun@gmail.com', '$2a$10$/HOt8euzARRNMAIEMEY3ouY.pPW8xJ8N/MhzjLZ0lGtm7i0MZrbLW', 'ROLE_USER');

INSERT INTO `project` (id, name, short_name, description, project_creator_id, active, project_type)
VALUES (1, 'SmartMan', 'SM', "Scrum managing app", 2, TRUE, "SCRUM"),
  (2, 'ChatBot', 'CB', "Booking rooms chatBot", 3, TRUE, "KANBAN"),
  (3, 'TravelApp', 'TA', "Trip planning app", 4, TRUE, "SCRUM"),
  (4, 'Facebook', 'FB', "Social network", 5, TRUE, "KANBAN"),
  (5, 'Spotify', 'SP', "free music app", 6, TRUE, "SCRUM"),
  (6, 'Youtube', 'YT', "video streaming platform", 7, TRUE, "KANBAN");

INSERT INTO `version` (id, name, description, start_date, release_date, project_id, released)
VALUES (1, 'V. 0.1', NULL, '2018-02-01', '2018-02-25', 1, FALSE),
  (2, 'V. 0.2', NULL, '2018-02-02', '2018-02-26', 2, FALSE),
  (3, 'V. 0.3', NULL, '2018-02-03', '2018-02-27', 3, FALSE),
  (4, 'V. 0.4', NULL, '2018-02-04', '2018-02-28', 4, FALSE),
  (5, 'V. 0.5', NULL, '2018-02-05', '2018-03-01', 5, FALSE),
  (6, 'V. 0.6', NULL, '2018-02-06', '2018-03-02', 6, FALSE);

INSERT INTO `sprint` (id, name, start_date, deadline, description, project_id, version_id, close_date)
VALUES (1, 'Start project SM', '2018-02-02', '2018-02-09', NULL, 1, 1, NULL),
  (2, 'Start project CB', '2018-02-02', '2018-02-09', NULL, 2, 2, NULL ),
  (3, 'Start project TA', '2018-02-02', '2018-02-09', NULL, 3, 3, NULL ),
  (4, 'Start project FB', '2018-02-02', '2018-02-09', NULL, 4, 4, NULL ),
  (5, 'Start project SP', '2018-02-02', '2018-02-09', NULL, 5, 5, NULL ),
  (6, 'Start project YT', '2018-02-02', '2018-02-09', NULL, 6, 6, NULL );

INSERT INTO `epic` (id, name, summary, status, created, updated, reporter_id, assignee_id, priority, description, project_id)
VALUES
  (1, 'DataBase',
      'It is necessary to create a database of the project with the filling of the initial test data.',
      'TO_DO', '2018-02-04 10:00:00', NULL, 7, NULL, 'MEDIUM', NULL, 1),
  (2, 'Start Back-End', 'Develop the basic functionality of the BACK-END parts of the project.',
      'TO_DO', '2018-02-04 11:00:00', NULL, 7, NULL, 'MEDIUM', NULL, 1),
  (3, 'Start Front-End', 'Develop the basic functionality of the FRONT-END parts of the project.',
      'TO_DO', '2018-02-04 11:05:00', NULL, 7, NULL, 'MEDIUM', NULL, 1);

INSERT INTO team VALUES
  (1, 'SmartManDevs'),
  (2, 'SmartManCustomer'),
  (3, 'SmartManTesters'),
  (4, 'SmartManBA');

INSERT INTO `team_project` (team_id, project_id) VALUES (1, 1), (2, 1), (3, 1), (4, 1);

INSERT INTO `team_user` (team_id, user_id)
VALUES (1, 2), (1, 3), (1, 4), (1, 5), (1, 6), (1, 7), (2, 8),(3, 1), (3, 2), (3, 3),
  (3, 8), (4, 2), (4, 6), (4, 5);

INSERT INTO task_list (id, name, project_id) VALUES
  (1,'BACKLOG', 1),
  (2,'TO_DO', 1),
  (3,'IN_PROGRESS', 1),
  (4,'IN_QA', 1),
  (5,'DONE', 1),
  (6,'BACKLOG', 3),
  (7,'TO_DO', 3),
  (8,'IN_PROGRESS', 3),
  (9,'IN_QA', 3),
  (10,'DONE', 3),
  (11,'BACKLOG', 5),
  (12,'TO_DO', 5),
  (13,'IN_PROGRESS', 5),
  (14,'IN_QA', 5),
  (15,'DONE', 5),
  (16,'toDo', 2),
  (17,'DoNe', 2),
  (18,'toDo', 4),
  (19,'DoNe', 4),
  (20,'toDo', 6),
  (21,'DoNe', 6);

INSERT INTO `issue` (id, summary, status, reporter_id, assignee_id, created, updated, priority, description,
                     story_points, sprint_id, project_id, type, epic_id, version_id, task_list_id)
VALUES
  -- project 1
  (1,
    'Perform a SCRUM domain analysis.','BACKLOG', 7, 5, '2018-02-02 11:00:00', NULL, 'MEDIUM', NULL, NULL, 1, 1, 'STORY', 1, 1, 1),
  (2, 'Create a script to fill the database with test data.', 'TO_DO', 7, 5, '2018-02-02 11:05:00',
      NULL, 'MEDIUM', NULL, NULL, 1, 1, 'STORY', 1, 1, 2),
  (3, 'Make the assignee_id field in the issue table with the NULL property.', 'IN_PROGRESS', 7, 5,
      '2018-02-03 12:15:00', NULL, 'MEDIUM', NULL, NULL, 1, 1, 'BUG', 1, 1, 3),
  (4, 'Develop a start-up BACK-END part of the project.', 'DONE', 7, 4, '2018-02-03 13:00:00',
      NULL, 'MEDIUM', NULL, NULL, 1, 1, 'STORY', 2, 1, 5),
  (5, 'Develop a start-up FRONT-END part of the project.', 'BACKLOG', 7, 2, '2018-02-03 13:05:00',
      NULL, 'MEDIUM', NULL, NULL, 1, 1, 'STORY', 3, 1, 1),
  (6, 'Implement Spring Security in the BACK-END part of the project.', 'BACKLOG', 7, 3,
      '2018-02-04 10:05:00', NULL, 'MEDIUM', NULL, NULL, 1, 1, 'STORY', 2, 1, 1),
  (31, 'Implement Attachments card field in BACK-END.', 'DONE', 1, 1,
      '2018-02-04 10:05:00', NULL, 'MEDIUM', NULL, NULL, 1, 1, 'STORY', 2, 1, 5),
  (32, 'Implement Attachments card field in FRONT-END.', 'IN_PROGRESS', 1, 1,
      '2018-02-04 10:05:00', NULL, 'MEDIUM', NULL, NULL, 1, 1, 'STORY', 2, 1, 3),
  (33, 'Implement html hrefs when hovering over the carousel arrows', 'IN_PROGRESS', 1, 1,
      '2018-02-04 10:05:00', NULL, 'MEDIUM', NULL, NULL, 1, 1, 'BUG', 2, 1, 3),
  (34, 'Implement Spring Security in the BACK-END part of the project.', 'IN_PROGRESS', 7, 3,
      '2018-02-04 10:05:00', NULL, 'MEDIUM', NULL, NULL, 1, 1, 'STORY', 2, 1, 3),
  (35, 'Implement Spring Security in the BACK-END part of the project.', 'DONE', 1, 1,
      '2018-02-04 10:05:00', NULL, 'MEDIUM', NULL, NULL, 1, 1, 'STORY', 2, 1, 5),
  (36, 'Implement Spring Security in the BACK-END part of the project.', 'TO_DO', 1, 1,
      '2018-02-04 10:05:00', NULL, 'MEDIUM', NULL, NULL, 1, 1, 'STORY', 2, 1, 2),
  (37, 'Implement Spring Security in the BACK-END part of the project.', 'IN_PROGRESS', 1, 1,
      '2018-02-04 10:05:00', NULL, 'MEDIUM', NULL, NULL, 1, 1, 'STORY', 2, 1, 3),
  (38, 'Implement Spring Security in the BACK-END part of the project.', 'TO_DO', 1, 1,
      '2018-02-04 10:05:00', NULL, 'MEDIUM', NULL, NULL, 1, 1, 'STORY', 2, 1, 2),
  (39, 'Implement Spring Security in the BACK-END part of the project.', 'TO_DO', 1, 1,
      '2018-02-04 10:05:00', NULL, 'MEDIUM', NULL, NULL, 1, 1, 'STORY', 2, 1, 2),
  (40, 'Implement Spring Security in the BACK-END part of the project.', 'DONE', 1, 1,
      '2018-02-04 10:05:00', NULL, 'MEDIUM', NULL, NULL, 1, 1, 'STORY', 2, 1, 5),
  -- project 2
  (7, 'done story', 'DONE', 7, 3, '2018-02-04 10:05:00', NULL, 'MEDIUM', "some descr", NULL, 2, 2, 'STORY', 2, 1, 16),
  (8, 'inProgress bug', 'IN_PROGRESS', 7, 3, '2018-02-04 10:05:00', NULL, 'HIGH', "some another description",
  NULL, 2, 2, 'BUG', 2, 1, 17),
  (9, 'backlog story', 'BACKLOG', 7, 3, '2018-02-04 10:05:00', NULL, 'LOW', "this is the description", NULL, NULL , 2,
  'STORY', 2, 1, 16),
  (10, 'toDo story', 'BACKLOG', 7, 3, '2018-02-04 10:05:00', NULL, 'MEDIUM', "descript", NULL, NULL , 2,
   'STORY', 2, 1, 17),
  -- project 3
  (11, 'something have to do', 'IN_PROGRESS', 7, 3, '2018-02-04 10:05:00', NULL, 'MEDIUM', "some description with some words",
       NULL, 3, 3, 'STORY', 2, 1, 8),
  (12, 'nice iu', 'TO_DO', 7, 3, '2018-02-04 10:05:00', NULL, 'HIGH', "task in the sprint", NULL, 3, 3,
   'STORY', 2, 1, 7),
  (13, 'read a book', 'TO_DO', 7, 3, '2018-02-04 10:05:00', NULL, 'LOW', "task in the backlog to project 3",
       NULL, 3, 3, 'STORY', 2, 1, 7),
  (14, 'go to the gym', 'BACKLOG', 7, 3, '2018-02-04 10:05:00', NULL, 'LOW', "task in the backlog to project 3",
       NULL, NULL, 3, 'STORY', 2, 1, 6),
  (15, 'climb a mountain', 'BACKLOG', 7, 3, '2018-02-04 10:05:00', NULL, 'MEDIUM',
       "task in the backlog to project 3", NULL, NULL, 3, 'STORY', 2, 1, 6),
  -- project 4
  (16, 'summary', 'TO_DO', 7, 3, '2018-02-04 10:05:00', NULL, 'MEDIUM', "descr for project #4 in sprint",
       NULL, 4, 4, 'STORY', 2, 1, 18),
  (17, 'summary #2 for project #4', 'TO_DO', 7, 3, '2018-02-04 10:05:00', NULL, 'HIGH',
       "descr for project #4 in sprint", NULL, 4, 4, 'STORY', 2, 1, 19),
  (18, 'summary #3 for project #4', 'DONE', 7, 3, '2018-02-04 10:05:00', NULL, 'LOW',
       "descr for project #4 in backlog", NULL, 4, 4, 'BUG', 2, 1, 18),
  (19, 'summary #4 for project #4', 'BACKLOG', 7, 3, '2018-02-04 10:05:00', NULL, 'HIGH',
       "descr for project #4 in backlog", NULL, NULL, 4, 'BUG', 2, 1, 19),
  (20, 'summary #5 for project #4', 'BACKLOG', 7, 3, '2018-02-04 10:05:00', NULL, 'MEDIUM',
       "descr for project #4 in backlog", NULL, NULL, 4, 'STORY', 2, 1, 18),
  -- project 5
  (21, 'summary #1 for project #5', 'DONE', 7, 3, '2018-02-04 10:05:00', NULL, 'MEDIUM',
       "descr for project #5 in sprint", NULL, 5, 5, 'STORY', 2, 1, 15),
  (22, 'summary #2 for project #5', 'TO_DO', 7, 3, '2018-02-04 10:05:00', NULL, 'HIGH',
       "descr for project #5 in sprint", NULL, 5, 5, 'STORY', 2, 1, 12),
  (23, 'summary #3 for project #5', 'IN_PROGRESS', 7, 3, '2018-02-04 10:05:00', NULL, 'LOW',
       "descr for project #5 in backlog", NULL, 5, 5, 'BUG', 2, 1, 13),
  (24, 'summary #4 for project #5', 'BACKLOG', 7, 3, '2018-02-04 10:05:00', NULL, 'HIGH',
       "descr for project #5 in backlog", NULL, NULL, 5, 'BUG', 2, 1, 11),
  (25, 'summary #5 for project #5', 'BACKLOG', 7, 3, '2018-02-04 10:05:00', NULL, 'MEDIUM',
       "descr for project #5 in backlog", NULL, NULL, 5, 'STORY', 2, 1, 11),
  -- project 6
  (26, 'summary #1 for project #6', 'DONE', 7, 3, '2018-02-04 10:05:00', NULL, 'MEDIUM',
       "descr for project #6 in sprint", NULL, 6, 6, 'STORY', 2, 1, 20),
  (27, 'summary #2 for project #6', 'TO_DO', 7, 3, '2018-02-04 10:05:00', NULL, 'HIGH',
       "descr for project #6 in sprint", NULL, 6, 6, 'STORY', 2, 1, 21),
  (28, 'summary #3 for project #6', 'IN_PROGRESS', 7, 3, '2018-02-04 10:05:00', NULL, 'LOW',
       "descr for project #6 in backlog", NULL, 6, 6, 'BUG', 2, 1, 20),
  (29, 'summary #4 for project #6', 'BACKLOG', 7, 3, '2018-02-04 10:05:00', NULL, 'LOW',
       "descr for project #6 in backlog", NULL, NULL, 6, 'BUG', 2, 1, 21),
  (30, 'summary #5 for project #6', 'BACKLOG', 7, 3, '2018-02-04 10:05:00', NULL, 'MEDIUM',
       "descr for project #6 in backlog", NULL, NULL, 6, 'STORY', 2, 1, 20);

INSERT INTO `project_user` (project_id, user_id, role) VALUES
  (1, 8, 'WRITER'),
  (1, 3, 'WRITER'),
  (1, 4, 'WRITER'),
  (1, 5, 'WRITER'),
  (1, 6, 'WRITER'),
  (1, 2, 'WRITER'),
  (1, 7, 'WRITER'),
  (2, 2, 'READER'),
  (3, 2, 'READER'),
  (4, 2, 'READER'),
  (5, 2, 'READER'),
  (6, 2, 'READER');



