$('#save').click(function () {

    var userinfo = {
        "username": $('#username').val(),
        "password": $('#pass').val(),
        "email": $('#mail').val(),
        "surname": $('#surname').val()
    }

    $.ajax({
        type: "POST",
        url: "/api/user",
        data: JSON.stringify(userinfo),
        contentType: "application/json; charset=utf-8",
        dataType: "json",

        complete: function (data) {
          window.location = "/toAdminPage";

        },

        failure: function (errMsg) {

            alert(errMsg);
        }
    });
});

$('#saveTeams').click(function () {

    var teaminfo = {
        "name": $('#nameTeam').val()
    }

    $.ajax({
        type: "POST",
        url: "/api/team",
        data: JSON.stringify(teaminfo),
        contentType: "application/json; charset=utf-8",

        complete: function () {
            window.location = "/toAdminPage";
        },

        failure: function (errMsg) {
            alert(errMsg);
        }
    });
});

var module = angular.module("MyApp", []);
module.controller("adminUpdateUser", function ($scope, $http) {
    $http
        .get('/api/user')
        .then(function (response) {
            $scope.users = response.data;


        });
    $http
        .get('/api/team')
        .then(function (response) {
            $scope.teams = response.data;
        })


    $scope.changeUser = function (user) {
        $scope.currentUser = user;

    };

    $scope.updateUser = function (user) {

        user.username = user.name;
        $http.put("/api/user/" + user.userId, user, user.userId)
            .then(function () {
                console.log("Success");
            })
        console.log($scope.currentUser)
    }


    $scope.addTeam = function (user,team) {

        $http.post("/api/user/" + user.userId + "/team/" + team.teamId )
            .then(function () {
                console.log("success")
            })

    }


});

module.controller("updateUserPassword", function ($scope, $http,$location) {
    $scope.updatePassword = function (password) {
        $http.put("/api/user/update/password/", password)
            .then(function () {
                console.log("success updating password");
               window.location.reload(true);
            })
    }
});



