"use strict";

var application = angular.module("app", ["ngRoute", "dndLists", "ngCookies"]);

application.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "templates/project_list.html",
            controller: "mainPageController"
        })
        .when("/project/:id/dashboard", {
            templateUrl: "templates/dashboard-new.html",
            controller: "dashboardPageController"
        })
        .when("/project/:id/backlog", {
            templateUrl: "templates/backlog.html",
            controller: "backlogPageController"
        })
        .when("/project/:id/teams", {
            templateUrl: "templates/team.html",
            controller: "teamPageController"
        })
        .otherwise({
            redirectTo: "/"
        })
    ;
});

application.controller("mainController", function ($scope, $window, $location, $timeout, $cookies, projectService, userService) {

    var carouselProjects;
    var activeIndex = 0;

    projectService.getAllProjects()
        .then(function success(response) {
            carouselProjects = response;

            if ($cookies.get("newProjectCreated") !== '0') {
                var newProjectFirstArray = [];
                newProjectFirstArray.push(carouselProjects[carouselProjects.length - 1]);
                for (var i = 0; i < carouselProjects.length - 1; i++) {
                    newProjectFirstArray.push(carouselProjects[i]);
                }

                carouselProjects = newProjectFirstArray;
            }
            $scope.projects = carouselProjects;
            projectService.saveProjectMetaData(carouselProjects[0]);

            $scope.projectId = $cookies.get("projectId");

            $("#projectCarousel").on('slid.bs.carousel', function () {
                var carouselItems = document.querySelectorAll(".carousel-item");
                for (var i = 0; i < carouselItems.length; i++) {
                    if (carouselItems[i].closest(".active")) {
                        activeIndex = i;
                        $cookies.put("newProjectCreated", 0);
                        $cookies.put("cardListId", undefined);
                    }
                }
            });

            $location.path("/project/" + $cookies.get("projectId") + "/dashboard");
        });

    userService.getCurrentUser()
        .then(function success(response) {
            //alert(JSON.stringify(response));
            $scope.currentUser = response;
            $cookies.put("currentUserFullName", response.fullName);
            $scope.firstUserNameLetter = response.name.charAt(0).toUpperCase();
        });

    $scope.createNewProject = function (project) {
        projectService.addProject(project);
        $cookies.put("newProjectCreated", 1);
        $window.location.reload();
    };

    $scope.deleteProject = function () {
        projectService.deleteProject($cookies.get("projectId"));
        $(".modal-backdrop").remove();
        $timeout(function () {
            $window.location.reload();
        }, 100);
    };

    $scope.getProjectURL = function (nextProject) {
        if (nextProject) {
            activeIndex++;
            if (activeIndex === carouselProjects.length) {
                activeIndex = 0;
            }
        }
        else {
            activeIndex--;
            if (activeIndex === -1) {
                activeIndex = carouselProjects.length - 1;
            }
        }
        projectService.saveProjectMetaData(carouselProjects[activeIndex]);

        var locationName = $location.url().split("/")[3];
        var projectId = $cookies.get("projectId");

        $scope.projectId = projectId;
        $location.path("/project/" + projectId + "/" + locationName);

    };

    $scope.initializeProjectId = function () {
        $scope.projectId = $cookies.get("projectId");
    };

});


application.controller("mainPageController", function ($location, $cookies) {
    if ($cookies.get("projectId")) {
        $location.path("/project/" + $cookies.get("projectId") + "/dashboard");
    }
});

application.controller("userModalController", function ($scope, $location, $cookies, projectService, issueService, userService) {
    projectService.getAllProjects()
        .then(function success(response) {
            $scope.projects = response;
            $scope.userNameDisplay = $cookies.get("currentUserFullName");
        });

    $scope.getUserIssues = function (prjId) {
        var projectLists = issueService.getProjectCardListsIssuesByProjectId(prjId);
        console.log(JSON.stringify(projectLists));
        var userProjectLists = [];

        projectLists.forEach(function (list) {
            list.forEach(function (value) {
                if (value.assigneeId === $cookies.get("currentUser").userId) {
                    userProjectLists.push(value);
                }
            })
        });

        $scope.projectIssues = userProjectLists;
    };

    $scope.updateUserPassword = function (password) {
        console.log(JSON.stringify(password));
        if (password.new === password.newRepeat) {
            userService.updateUserPass(password.new);
        }
        else {
            alert("Passwords do no match");
        }
    }

});


application.controller("dashboardPageController", function ($scope, $route, $location, $cookies, $timeout, $window, issueService, projectService) {
    var projectUsers;
    var projectLists;

    issueService.getProjectCardLists().then(function success(response) {
        projectLists = response;
        $scope.cardLists = projectLists;
        projectLists.forEach(function (value) {
            if(value.name === 'TO_DO'){
                $scope.toDoList = value;
            }
            else if(value.name === 'IN_PROGRESS'){
                $scope.inProgressList = value;
            }
            else if(value.name === 'IN_QA'){
                $scope.inQAList = value;
            }
        });
         projectService.getProjectUsers()
            .then(function (value) {
                $scope.projectUsers = value;
                projectUsers = value;
            });

    });

    projectService.projectIsCustomizable()
        .then(function success(response) {
            $scope.projectIsCustomizable = response;
    });

	$scope.addIssue = function (issue) {
		issue.sprintId = $cookies.get("currentSprintId");
		issueService.addIssue(issue);
		$(".modal-backdrop").remove();
		$timeout(function () {
			$route.reload(true);
		}, 250);
	};

	$scope.closeCurrentSprint = function () {
		issueService.closeCurrentSprint(projectLists);
		$(".modal-backdrop").remove();
		$timeout(function () {
			$location.path("/project/" + $cookies.get("projectId") + "/backlog");
		}, 500);
	};

	$scope.editCard = function (card) {
		projectUsers.forEach(function (prj_user, index, array) {
			if (card.assignee === prj_user.user) {
				card.assigneeId = prj_user.userId;
			}
		});
		issueService.editCard(card);
        $(".modal-backdrop").remove();
        $timeout(function () {
            $route.reload();
        }, 500);
	};

	$scope.rememberListName = issueService.rememberListName;

    $scope.checkForItemsNotDone = function(){
        issueService.checkForItemsNotDone(projectLists);
    };
    $scope.changeStatus = issueService.changeStatus;
    $scope.rememberListName = issueService.rememberListName;
    $scope.changeCardList = issueService.changeCardList;

    $scope.saveCardListId = function (cardListId) {
        $cookies.put("cardListId", cardListId);
    };

    $scope.createNewCardList = function (cardListName) {
        issueService.createNewCardList(cardListName);
        $route.reload();
    };

    $scope.changeCardListName = projectService.changeCardListName;

});

application.controller("backlogPageController", function ($scope, $route, $location, $cookies, $timeout, backlogService, issueService) {
    backlogService.getBacklog()
        .then(function success(response) {
            $scope.backlog = response;
            $scope.projectIdHTML = $cookies.get("projectId");
        });

    $scope.addTask = function (task) {
        issueService.getProjectCardLists().then(function success(response) {
            var projectLists = response;
            var listId;
            listId = projectLists[0].id;
            backlogService.addIssueToBackLog(task, listId);
            $route.reload(true);
            $(".modal-backdrop").remove();
        });
    };

    $scope.saveNewSprint = function () {
        issueService.getProjectCardLists().then(function success(response) {
            backlogService.saveNewSprint(response[0].id, response[1].id);
        });
        $timeout(function () {
            $location.path("/project/" + $cookies.get("projectId") + "/dashboard");
        }, 700);
        $(".modal-backdrop").remove();
    };

    $scope.pushToBacklog = backlogService.putBackToBacklog;
    $scope.checkIfAnySprintIsOpen = backlogService.checkIfAnySprintIsOpen;
});

application.controller("teamPageController",
    function ($scope, $http, $location, $timeout, $cookies, projectService, teamService) {

        projectService.getProjectTeams()
            .then(function success(response) {
                $scope.projectTeams = response;
            });

        teamService.getAllTeams()
            .then(function (value) {
                $scope.allTeams = value;
            });

        $scope.addTeamToProject = function (team) {
            teamService.addTeamToProject(team);
            $(".modal-backdrop").remove();
        };

        $scope.removeTeamFromProject = projectService.removeTeamFromProject;


    });
