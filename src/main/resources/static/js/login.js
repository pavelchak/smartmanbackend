$('#login-btn').click(function () {
    var cred = {
        "username": $('#email').val(),
        "password": $('#password').val()
    };
    $.ajax({
        type: "POST",
        url: "/login",
        data: JSON.stringify(cred),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        complete: function (res) {

            window.location = "/";

            if (res.responseJSON.status == 401) {
                window.location = "/loginerror";
            }
        },
        failure: function (errMsg) {
            alert(errMsg);
        }
    });
});