application.factory("projectService", function ($http, $q, $location, $route, $cookies, $window) {

    return {

        getAllProjects: function () {
            var defered = $q.defer();
            $http.get("/api/project")
                .then(function (response) {
                    defered.resolve(response.data);
                });
            return defered.promise;
        },

        addProject: function(project) {
            project.active = true;
            if (project.projectType) {
                project.projectType = "SCRUM";
            } else {
                project.projectType = "KANBAN";
            }
            $http.post("/api/project", project)
                .then(function(response) {
                });
        },

        getProjectTeams: function () {
            var defered = $q.defer();
            $http.get("/api" + $location.url())
                .then(function success(response) {
                    defered.resolve(response.data);
                });
            return defered.promise;
        },

        removeTeamFromProject: function (teamId) {
            var url = "/api/project/" + 1 + "/team/" + teamId;
            console.log(url);
            $http.delete(url)
                .then(function success(response) {
                    $(".modal-backdrop").remove();
                    $route.reload();
                });
        },

        saveProjectMetaData: function (prj) {
            $cookies.put("projectId", prj.projectId);
            $cookies.put("currentSprintId", prj.currentSprintId);
        },

        deleteProject: function (id) {
            $http.get("/api/project/" + id)
                .then(function (value) {
                    var prj = value.data;
                    prj.active = false;

                    $http.post("/api/project/" + id, prj)
                        .then(function (response) {
                        })
                });
        },

        getProjectUsers: function () {
            var deferred = $q.defer();
            $http.get("/api/project/" + $cookies.get("projectId") + "/projectUsers")
                .then(function (value) {
                    deferred.resolve(value.data);
                });
            return deferred.promise;
        },

        projectIsCustomizable: function() {
            var defered = $q.defer();
            $http.get("/api/project/" + $cookies.get("projectId") + "/isScrum")
                .then(function success(response) {
                    defered.resolve(response.data);
                });
            return defered.promise;
        },

        changeCardListName: function(cardListId, cardListName) {
            $http.put("/api/project/" + $cookies.get("projectId") + "/card-list/" + cardListId + "/" + cardListName)
                .then(function success() {
                });
        }

    };

});