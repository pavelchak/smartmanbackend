application.factory("backlogService", function ($http, $q, $location, $cookies) {
    var backlog;

    return {

        getBacklog: function () {

            var defered = $q.defer();
            var url = $location.url();

            $http.get("/api/project/" + $cookies.get("projectId") + "/card-lists")
                .then(function success(response) {
                    backlog = {
                        groups: {
                            "Backlog": [],
                            "NextSprint": []
                        }
                    };

                    var issuesLists = response.data;
                    issuesLists.forEach(function (issueList) {
                        if (issueList.name === 'BACKLOG') {
                            issueList.issues.forEach(function (value) {
                                backlog.groups.Backlog.push(value);
                            });
                        }
                        else {
                            issueList.issues.forEach(function (issue) {
                                if (issue.sprintId === null) {
                                    backlog.groups.NextSprint.push(issue);
                                }
                            });
                        }

                    });
                    defered.resolve(backlog);
                });

            return defered.promise;
        },

        addIssueToBackLog: function (issue, listId) {
            issue.projectId = $cookies.get("projectId");
            issue.status = "BACKLOG";
            issue.sprintId = null;
            issue.taskListId = listId;
            console.log(JSON.stringify(issue));
            $http.post("/api/project/" + issue.projectId + "/issue", issue)
                .then(function success() {
                }, function error() {
                    console.log("fail");
                });

        },

        putBackToBacklog: function (issue) {
            backlog.groups.Backlog.push(issue);
            var elementIndexToRemove = backlog.groups.NextSprint.indexOf(issue);
            if (elementIndexToRemove > -1) {
                backlog.groups.NextSprint.splice(elementIndexToRemove, 1);
            }
        },

        checkIfAnySprintIsOpen: function () {
            $http.get("api/project/" + $cookies.get("projectId"))
                .then(function (response) {
                    var prj = response.data;

                    if (prj.currentSprintId) {
                        $("#alertCloseCurrentSprint").removeClass("d-none");
                        $("#createNewSprint").addClass("disabled").prop('disabled', true);
                    }
                    else {
                        console.log("in disable");
                        $("#alertCloseCurrentSprint").addClass("d-none");
                        $("#createNewSprint").removeClass("disabled").prop('disabled', false);
                    }
                });
        },

        saveNewSprint: function (backlogId, toDoId) {
            var currentProjectId = $cookies.get("projectId");
            var nextSprintBody = {
                name: "next sprint",
                startDate: new Date(),
                deadline: new Date().setDate(new Date().getDate() + 14)
            };
            $http.post("/api/project/" + currentProjectId + "/sprint/version/1", nextSprintBody)
                .then(function success(response) {
                    var newSprint = response.data;
                    var nextSprintIssues = backlog.groups.NextSprint;

                    nextSprintIssues.forEach(function success(item, i, nextSprintIssues) {
                        if (item.status === "BACKLOG") {
                            item.status = "TO_DO";
                        }
                        if(item.taskListId === backlogId){
                            item.taskListId = toDoId;
                        }
                        item.sprintId = newSprint.sprintId;

                        $http.post("/api/project/" + currentProjectId + "/issue/" + item.issueId, item)
                            .then(function success(value) {
                                console.log("post: item:" + item.issueId + item.sprintId);
                            }, function (reason) {

                            });
                    });

                }, function (reason) {
                    console.log(JSON.stringify(reason));
                });
        }
    };
});