application.factory("teamService", function($http, $q, $cookies, $route) {

    return {

        getTeamMembers: function(teamId) {
            var defered = $q.defer();
            console.log("teamId: " + teamId);
            $http.get("/api/team/" + teamId + "/users")
                .then(function success(response) {
                    defered.resolve(response.data);
                });
            return defered.promise;
        },

        getAllTeams: function () {
            var defered = $q.defer();
            $http.get("/api/team")
                .then(function (value) {
                    defered.resolve(value.data);
                });
            return defered.promise;
        },

        addTeamToProject : function (team) {
            $http.patch("/api/project/" + $cookies.get("projectId") + "/team/" + team.teamId)
                .then(function (value) {
                    $route.reload();

                });
        }

    };

});