var lastDroppedInListName;

application.factory("issueService", function ($http, $q, $location, $route, $cookies) {
    var dashboard;

    return {

        getIssues: function () {

            var deferred = $q.defer();
            var url = $location.url();

            $http.get("/api" + url)
                .then(function success(response) {
                    dashboard = {
                        selected: null,
                        issueGroups: {
                            "ToDo": [],
                            "InProgress": [],
                            "InQA": [],
                            "Done": []
                        }
                    };
                    var issues = response.data;
                    issues.forEach(function (issue, i, issues) {
                        switch (issue.status) {
                            case "TO_DO":
                                dashboard.issueGroups.ToDo.push(issue);
                                break;
                            case "IN_PROGRESS":
                                dashboard.issueGroups.InProgress.push(issue);
                                break;
                            case "IN_QA":
                                dashboard.issueGroups.InQA.push(issue);
                                break;
                            case "DONE":
                                dashboard.issueGroups.Done.push(issue);
                                break;
                            default:
                                dashboard.issueGroups.ToDo.push(issue);
                        }
                    });
                    deferred.resolve(dashboard);
                });

            return deferred.promise;
        },

        getProjectCardListsIssuesByProjectId: function(prjId) {
            var deferred = $q.defer();

            $http.get("/api/project/" + prjId + "/card-lists")
                .then(function success(response) {
                    var cardLists = response.data;
                     deferred.resolve(cardLists);
                });

            return deferred.promise;
        },


        addIssue: function (issue) {
            issue.projectId = $cookies.get("projectId");
            issue.taskListId = $cookies.get("cardListId");
            issue.status = "TO_DO";
            issue.sprintId = $cookies.get("currentSprintId");
            $http.post("/api/project/" + $cookies.get("projectId") + "/issue", issue)
                .then(function success() {

                }, function error() {

                });
        },

        changeStatus: function (issueId) {
            $http.put("/api/project/" + $cookies.get("projectId") + "/issue/" + issueId + "/" + lastDroppedInListName)
                .then(function success() {
                });
        },

        rememberListName: function (cardListId) {
            lastDroppedInListName = cardListId;
            $cookies.put("cardListId", cardListId);
        },

        editCard: function (card) {
            if($cookies.get("cardListId") !== undefined){
                card.taskListId = $cookies.get("cardListId");
            }
            $http.post("/api/project/" + card.projectId + "/issue/" + card.issueId, card)
                .then(function success() {

                }, function fail(response) {

                });
        },

        checkForItemsNotDone: function (lists) {
            var tdl;
            var ipl;
            var iql;
            lists.forEach(function (value) {
                if(value.name === 'TO_DO'){
                    tdl = value;
                }
                else if(value.name === 'IN_PROGRESS'){
                    ipl = value;
                }
                else if(value.name === 'IN_QA'){
                    iql = value;
                }
            });
            var notDoneIssuesNumber = tdl.length + ipl.length + iql.length;

            if (notDoneIssuesNumber) {
                $("#alertNotDoneItemsInSprint").removeClass("d-none");
                $("#currentlyNotDoneText").removeClass("d-none");
            }
            else {
                $("#alertNotDoneItemsInSprint").addClass("d-none");
                $("#currentlyNotDoneText").addClass("d-none");
            }
        },

        closeCurrentSprint: function (lists) {
            var tdl;
            var ipl;
            var iql;
            lists.forEach(function (value) {
                if(value.name === 'TO_DO'){
                    tdl = value.issues;
                }
                else if(value.name === 'IN_PROGRESS'){
                    ipl = value.issues;
                }
                else if(value.name === 'IN_QA'){
                    iql = value.issues;
                }
            });
            var notDoneIssues = tdl.concat(ipl, iql);

            $http.get("/api/project/" + $cookies.get("projectId"))
                .then(function success(response) {
                    var currentProject = response.data;

                    $http.put("/api/project/" + $cookies.get("projectId") + "/closesprint/" + currentProject.currentSprintId)
                        .then(function success(response) {
                            notDoneIssues.forEach(function (item, index) {
                                item.sprintId = null;

                                $http.post("/api/project/" + $cookies.get("projectId") + "/issue/" + item.issueId, item)
                                    .then(function success(value) {
                                    }, function (reason) {

                                    });
                            })
                        });
                });
        },

        getProjectCardLists: function() {
            var deferred = $q.defer();
            var projectId = $cookies.get("projectId");

            $http.get("/api/project/" + projectId + "/card-lists")
                .then(function success(response) {
                    var cardLists = response.data;
                    deferred.resolve(cardLists);
                });
            return deferred.promise;
        },

        createNewCardList: function(cardListName) {
            $http.post("/api/project/" + $cookies.get("projectId") + "/card-list/" + cardListName)
                .then(function success() {
                });
        },

        changeCardList: function(cardId) {
            $http.put("/api/project/" + $cookies.get("projectId") + "/card-list/" + lastDroppedInListName + "/card/" + cardId)
                .then(function success(response) {
                });
        }

    };
});