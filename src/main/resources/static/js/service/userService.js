application.factory("userService", function ($http, $q, $location, $route, $cookies) {
    return {

        getTeamMembers: function () {
            var defered = $q.defer();
            $http.get("/api" + $location.url())
                .then(function success(response) {
                    defered.resolve(response.data);
                });
            return defered.promise;
        },

        removeMemberFromTeam: function (userId) {
            $http.delete("/api/project/" + projectId + "/user/" + userId)
                .then(function success() {
                    $route.reload(true);
                });
        },

        getCurrentUser: function () {
            var defered = $q.defer();
            $http.get("/api/authorized-user")
                .then(function success(response) {
                    defered.resolve(response.data);
                });
            return defered.promise;
        },

        updateUserPass: function (updatedPass) {
            console.log(JSON.stringify(updatedPass));
            $http.put("/api/user/update/password/", updatedPass)
                .then(function () {
                    alert("Success");
                })
        }

    };

});